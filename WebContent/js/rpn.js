var moduleProfile="ori-oai-profile";
var localAdr=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var mediaFolder=localAdr+"/"+moduleProfile+"/media/";
var recomLink=localAdr+"/ori-oai-search/thematic-search.html?menuKey=all&submenuKey=profile&init=true";

//recuperation des formation
var forms=document.getElementById("forms").textContent;
forms=forms.replace("[", "");
forms=forms.replace("]", "");
aForms=forms.split(",");


var urlform="";
for(h=0;h<aForms.length;h++){
	urlform=urlform+"form="+aForms[h]; 
	if(h!=aForms.length-1){
		urlform=urlform+"&";
	}
}

var url =localAdr+"/"+moduleProfile+"/rest/rpn/formations?"+urlform;
setRPN(url);

function setRPN(url) {
	var xhr1 = new XMLHttpRequest();
	xhr1.onload = function() {
		if (xhr1.readyState == 4) {
			if(xhr1.status === 200){
				var response = JSON.parse(xhr1.responseText);

				for (i = 0; i < 3; i++) {
					
					document.getElementById("bloc-rpn-"+i).style.backgroundImage=
						  "url("+mediaFolder+"background_rpn.png"+")";

					var title= document.getElementById("title-rpn-"+i);
					title.textContent = formatTitle(response.documents[i].title); 
					title.href = response.documents[i].url;
					
					var author= document.getElementById("author-rpn-"+i);
					if(author!=null){
					document.getElementById("author-rpn-label-"+i).textContent = "Auteurs : ";
					author.textContent = formatStringList(response.documents[i].authors);
					}

					var type= document.getElementById("type-rpn-"+i);
					if(type!=null){
					document.getElementById("type-rpn-label-"+i).textContent = "Type : ";
					type.textContent = formatStringList(response.documents[i].types);
					}
					
					document.getElementById("bloc-rpn-"+i).style.display="inline-block";
				}
				
				if(document.getElementById("other-rpn-link")!=null)
					document.getElementById("other-rpn-link").href=recomLink;
				if(document.getElementById("icon-other-rpn")!=null)
					document.getElementById("icon-other-rpn").src=mediaFolder+"/icon_autres_rpn.png";
				if(document.getElementById("index-other-rpn")!=null)
					document.getElementById("index-other-rpn").style.display="block";
				if(document.getElementById("result-other-rpn")!=null)
					document.getElementById("result-other-rpn").style.display="inline-block";
			}
		}
	}
	xhr1.open('GET',url );
	xhr1.send();
} 

function formatTitle(title){
	var titleSize = title.length;
	if(titleSize>73){
		return title.substr(0,70)+"...";
	}
	return title; 
}

function formatStringList(list){
	var max=2;
	if(list.length<max){
		max=list.length;
	}
	var str="";
	for( j=0;j<max;j++){
		str=str+list[j];
		if(j!=max-1){
			str=str+", ";
		}
	}
	return str
}
