<%@ include file="include.jsp" %>
<%@ page pageEncoding="UTF-8" %>


<div id="blocs-rpn" style="width: 100%;text-align: center;margin-top:5px;">

	<c:forEach var="i" begin="0" end="2">

		<div id="bloc-rpn-${i}" class="bloc-rpn"
			style="
			width: 220px; 
			height: 136px; 
			display: none; 
			vertical-align: top; 
			font-size: 15px; 
			text-align: left; 
			background-repeat: no-repeat; 
			background-size: cover; 
			background-position: 0px -2px;
			">
			
			<div style="margin-top: 5px; margin-left: 12px; margin-right: 5px;">
				<a class="title-rpn" id="title-rpn-${i}" href=""></a>
			</div>

		</div>

	</c:forEach>

	<div id="result-other-rpn" class="more-rpn-button"
		style="
		width: 170px; 
		height: 135px; 
		border: 1px solid; 
		display: none; 
		vertical-align: top; 
		font-size: 18px;
		">
		
		<div style="text-align: center; margin-top: 30px;">
			<a id="other-rpn-link" href=""> Découvrir d'autres
				recommandations </a>
		</div>
	</div>

</div>  

<c:import url="script.jsp"/>


