<%@ include file="include.jsp" %>

<div id="blocs-rpn" style="width: 100%;text-align:center">

<div>

<c:forEach var = "i" begin = "0" end = "2">

<div id="bloc-rpn-${i}"
    style="
    width: 306px;
    height: 188px;
    display: none;
    vertical-align: top;
    text-align: left;
	background-repeat: no-repeat;
	background-size: cover;
	background-position: 0px -4px;
    ">
    <div style="margin-left:20px;margin-right: 5px;margin-top: 5px;">
	    <div style="font-size:16px;">
	    	<a id="title-rpn-${i}" class="title-rpn" href="#"></a>
	    </div>
	    <div style="margin-top:20px;">
	    	<span id="author-rpn-label-${i}"></span>
	    	<b id="author-rpn-${i}"></b>
	    </div>
	    <div>
		<span id="type-rpn-label-${i}"></span>
	    	<b id="type-rpn-${i}"></b>
	    </div>
    </div>
</div>

</c:forEach>

</div>

<div id="index-other-rpn" class="other-rpn-button" style="margin-top: 50px;display:none">
	<a id="other-rpn-link" href="">
		<img id="icon-other-rpn" src="" 
			style="vertical-align:middle;margin-bottom: 17px;width: 100px;" />
		<span style="font-size: 18px;font-weight: 700;margin-left: -9px;">
			Voir d'autres ressources...
		</span>
	</a>
</div>

</div>  


<c:import url="script.jsp"/>