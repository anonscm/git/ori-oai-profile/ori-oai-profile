<%@ include file="include.jsp" %>

<c:choose>
	  <c:when test="${not empty param.forms && param.forms!=''}">
		  <c:import url="index-connected.jsp"/>
	  </c:when>
	  <c:otherwise>
		 <c:import url="index-anonymous.jsp"/>
	  </c:otherwise>
</c:choose>
