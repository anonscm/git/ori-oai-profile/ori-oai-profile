<%@ include file="include.jsp"%>


<div style="width: 100%; text-align: center;">

	<div style="margin-bottom: 30px;">

		<c:forEach var="i" begin="0" end="2">
			<div id="bloc-form-${i}"
				style="display: inline-block; vertical-align: top;">
				<div style="min-height: 100px;">
					<a id="result-link-${i}" href=""> </a>
				</div>
				<div style="margin-top: -100px; height: 100px;">
					<b id="count-form-${i}" style="font-size: 18px;"></b>
				</div>
			</div>
		</c:forEach>

	</div>
<c:if test="${param.auth == 'false'}">
	<c:import url="login-bloc.jsp" />
</c:if>

<script>
var moduleProfile="ori-oai-profile";
var extImg=".png";
var localAdr=location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var mediaFolder=localAdr+"/"+moduleProfile+"/media/";
var resultPage=localAdr+"/ori-oai-search/thematic-search.html?menuKey=all&init=true&submenuKey=";
var host= localAdr+"/"+moduleProfile+"/rest/rpn/total/";
var indexallUrl =localAdr+"/"+moduleProfile+"/rest/formation/indexall";

var xhr = new XMLHttpRequest(); 
xhr.onload = function() {
 if (xhr.readyState == 4) {
        var response = JSON.parse(xhr.responseText);
       
        var ibloc = Math.floor(Math.random()*3);
        

        for(i=0;i<response.length;i++){
        	var num = response[ibloc].length;
        	var random = Math.floor(Math.random()*num);
        	console.log(random);
        	var id = response[ibloc][random]
        	
        	document.getElementById("result-link-"+i).href = resultPage+id;
        	document.getElementById("result-link-"+i).innerHTML=
        		"<img id=\"img-form-"+i+"\" src=\""+mediaFolder+id+extImg+"\"/>";
        	setCount(host+id,"count-form-"+i);
        	
        	ibloc++;
        	if (ibloc>2){
        		ibloc=0;
        	}
 		}
   }
}

xhr.open('GET', indexallUrl);
xhr.send();


function setCount(url, count) {
	var xhr1 = new XMLHttpRequest();
    xhr1.onload = function() {
		if (xhr1.readyState == 4) {
			if(xhr1.status===200){
			var response = JSON.parse(xhr1.responseText);
			document.getElementById(count).textContent = response.total+" ressources";
			}
    	}
	}
	xhr1.open('GET',url );
	xhr1.send();
}
</script>
</div>


