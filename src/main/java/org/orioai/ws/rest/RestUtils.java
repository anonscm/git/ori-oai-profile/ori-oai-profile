package org.orioai.ws.rest;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class RestUtils {
	  /**
     * 
     * @return
     */
    public static Response getNotFoundResponse(){
    	return Response.status(Status.NOT_FOUND)
				.header("Access-Control-Allow-Origin", "*")
				.build();
    }
    
    /**
     * 
     * @return
     */
    public static Response getBadReqResponse(){
    	return Response.status(Status.BAD_REQUEST)
				.header("Access-Control-Allow-Origin", "*")
				.build();
    }
    
    public static Response getResponse(String str){
    	return Response.status(Status.OK)
    			.header("Access-Control-Allow-Origin", "*")
    			.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON+"; charset=utf-8")
    			.entity(str).build();
    }
    

}
