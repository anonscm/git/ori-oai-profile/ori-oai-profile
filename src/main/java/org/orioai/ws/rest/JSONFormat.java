/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.ws.rest;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.orioai.profile.entity.Formation;
import org.orioai.profile.entity.RPN;

/**
 * Classe pour transformer des objet au forrmat JSON
 * @author kruczek
 *
 */
public class JSONFormat {

	/**
	 * Transforme la liste de RPN du formation en JSON
	 * @param formId l'id de la formation
	 * @param totalCountDB le nombre total de RPN disponible en BDD
	 * @param rpns les RPN
	 * @return les informations au format JSON
	 */
	public static JSONObject getRPNFormation(String formId, long totalCountDB, List<RPN> rpns){
		JSONArray docsJSON = new JSONArray();
    	for(RPN rpn : rpns){
    		docsJSON.put(JSONFormat.getRPN(rpn));
    	}
    	
    	JSONObject formJSON = new JSONObject();
    	formJSON.put("formation-id", formId);
		formJSON.put("total",  totalCountDB);
		formJSON.put("documents", docsJSON);
		
		return formJSON;
	}
	
	public static JSONObject getRPNFormations(List<String> formIds, long totalCountDB, List<RPN> rpns){
		JSONArray docsJSON = new JSONArray();
    	for(RPN rpn : rpns){
    		docsJSON.put(JSONFormat.getRPN(rpn));
    	}
    	
    	JSONObject formJSON = new JSONObject();
    	formJSON.put("formation-id", getJSONArray(formIds));
		formJSON.put("total",  totalCountDB);
		formJSON.put("documents", docsJSON);
		
		return formJSON;
	}
	
	/**
	 * Tranforme une RPN en JSON
	 * @param rpn la RPN
	 * @return la RPN en JSON
	 */
	public static JSONObject getRPN(RPN rpn){
		JSONObject json = new JSONObject();
		
    	json.put("id", rpn.id);	
    	json.put("title", rpn.title);
    	json.put("description",  rpn.description);
    	json.put("url",  rpn.url);
    	json.put("date",  rpn.date);   	
    	json.put("authors",  getJSONArray(rpn.authors));
    	json.put("types",  getJSONArray(rpn.types));
    	json.put("url-picture", rpn.urlPicture);
    	
    	
    	return json;
	}
	
	/**
	 * Transforme une formation au format JSON
	 * @param temp la formation
	 * @return la formation en JSON
	 */
	public static JSONObject getFormation(Formation temp){
		JSONObject formJSON = new JSONObject();
		
    	formJSON.put("id", temp.getId());
    	formJSON.put("name", temp.getName());
    	formJSON.put("deweys", temp.getDeweys());
    	formJSON.put("keywords", temp.getKeywords());
    	
    	return formJSON;
	}
	
	/**
	 * Transforme une liste de formation en JSON
	 * @param temps les formations
	 * @return les formation en JSON
	 */
	public static JSONArray getFormations(List<Formation> temps){
		JSONArray jArray = new JSONArray();
		
		for (Formation temp : temps){
			JSONObject json = new JSONObject();
			json.put("id", temp.getId());
			json.put("name", temp.getName());
			jArray.put(json);
		}
		
		return jArray;
	}
	
	
	public static JSONArray getJSONArray(List<String> list){
		JSONArray jarray = new JSONArray();
    	for(String s : list){
    		jarray.put(s);
    	}
    	return jarray;
	}
	
	
	public static JSONObject getRPNWithWords(List<RPN> rpns, long totalCountDB, List<String> words){
		JSONArray rpnsJSON = new JSONArray();
    	for(RPN rpn : rpns){
    		rpnsJSON.put(JSONFormat.getRPN(rpn));
    	}
    	
    	JSONObject formJSON = new JSONObject();
    	formJSON.put("words", getJSONArray(words));
		formJSON.put("total",  totalCountDB);
		formJSON.put("documents", rpnsJSON);
		
		return formJSON;
	}
	
	public static JSONArray getListIdFormations(List<ArrayList<String>> blocs){
		JSONArray blocsJSON = new JSONArray();
		for(List<String> bloc : blocs) {
			JSONArray idsJSON = new JSONArray();
			for (String id : bloc){
	    		idsJSON.put(id);
	    	}
			blocsJSON.put(idsJSON);
		}
		return blocsJSON;
	}
}
