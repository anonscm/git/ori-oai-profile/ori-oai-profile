/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.ws.rest;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.orioai.profile.FormationService;
import org.orioai.profile.entity.Formation;
import org.orioai.profile.entity.IndexingResponse;
import org.orioai.profile.entity.RPN;
import org.orioai.profile.indexing.IndexingQueryFormat;
import org.orioai.profile.indexing.IndexingService;


/**
 * Classe qui represente l'interface des RPN /rpn
 * @author kruczek
 *
 */
@Path("rpn")
public class RPNWebInterface {
	public static IndexingService is = new IndexingService();
	public static  FormationService ts;
	
	public static void setIndexingService(IndexingService iServ){
		is=iServ;	
	}
	
	public static void setFormationService(FormationService tServ){
		ts=tServ;
	}
	
	/**
	 * Retourne les RPN qui contiennent les expressions
	 * Appelee par moodle
	 * @param query la requete
	 * @return les RPN en JSON
	 */
	@GET
    @Path("search")
	@Produces({"application/json"+ ";charset=utf-8"})
    public Response searchRPN(@QueryParam("w") List<String> words, 
    		@QueryParam("start") int start, 
    		@QueryParam("end") int end){

		if(!goodStartEnd(start, end))
			return RestUtils.getBadReqResponse();	
		
    	IndexingResponse ir = is.getRPNsWithWords(words, start+1, end+1);  
    	if(ir==null || ir.rpns.size()==0)
    		return RestUtils.getNotFoundResponse();
    	
    	JSONObject json =JSONFormat.getRPNWithWords(ir.rpns, ir.totalDoc, words); 
 
    	return RestUtils.getResponse(json.toString());
    }
    
    /**
     * Retourne les informations d'une RPN (id, titre, auteur...)
     * Appelee par moodle
     * @param id id de la RPN
     * @return les informations en JSON
     */
	@GET
	@Path("{id}")
	@Produces({"application/json"+ ";charset=utf-8"})
    public Response getRPN(@PathParam("id") String id) {
    	
    	RPN rpn = is.getRPNWithId(id);
    	
    	if(rpn==null)
    		return RestUtils.getNotFoundResponse();
    		
    	JSONObject json = JSONFormat.getRPN(rpn);
    	return RestUtils.getResponse(json.toString());
    }
    
    /**
     * Retourne les RPN d'une formation
     * Appelee par moodle
     * @param id ID de la formation
     * @return les RPN en JSON
     */
    @GET
	@Path("formation-{id}")
    @Produces({"application/json"+ ";charset=utf-8"})
    public Response getRPNForFormation(@PathParam("id") String id,
    		@QueryParam("start") int start, 
    		@QueryParam("end") int end) {
    	
    	if(!goodStartEnd(start, end))
    		return RestUtils.getBadReqResponse();
    		
    	Formation temp = ts.getFormation(id);
    	if(temp==null)
    		return RestUtils.getNotFoundResponse();
    	 	
    	IndexingResponse iResponse = is.getRPNsWithFormations(Arrays.asList(temp), start+1, end+1);

    	JSONObject json =JSONFormat.getRPNFormation(id, iResponse.totalDoc, iResponse.rpns);
    	return RestUtils.getResponse(json.toString());
    }
    
    /**
     * Retourne la requete utilisé sur l'indexing en fonction des formations
     * @param formId les formations
     * @return la requete
     */
    @GET
	@Path("query/{form-id}")
    @Produces({"text/plain"+ ";charset=utf-8"})
    public Response getIndexingQuery(@PathParam("form-id") String formId){
    	Formation temp = ts.getFormation(formId);
    	if(temp==null)
   		 return Response.status(Status.NOT_FOUND).build();
    	
    	String query = IndexingQueryFormat.getQuery(temp.getDeweys(),
    			temp.getKeywords(), 
    			temp.getGreen(), 
    			temp.getRed());
    	return Response.status(Status.OK).entity(query).build();
    }
    
    /**
     * Retourne les RPN de plusieurs formations
     * Appelee depuis javascript (xhr)
     * @param ids liste des ids des formations
     * @return les RPN en JSON
     */
    @GET
	@Path("formations")
    @Produces({"application/json"+ ";charset=utf-8"})
    public Response getRPNOfStudent(@QueryParam("form") List<String> ids) {
    		
    	List<Formation> temps = ts.getFormations(ids);
    	if(temps.size()==0)
    		return RestUtils.getNotFoundResponse();
    	
    	IndexingResponse ir = is.getRPNsWithFormations(temps, 1, 3);
    	if(ir==null || ir.rpns.size()==0)
    		return RestUtils.getNotFoundResponse();

    	JSONObject json =JSONFormat.getRPNFormations(ids, ir.totalDoc, ir.rpns);
    	return RestUtils.getResponse(json.toString());
    }
    
    /**
     * Retourne le nombre total de docuement en bdd pour la formation
     * Appelee depuis javascript (xhr)
     * @param formId la formation
     * @return le nombre de doc 
     */
    @GET
    @Path("total/{form-id}")
    @Produces({"application/json"+ ";charset=utf-8"})
    public Response getTotal(@PathParam("form-id") String formId){
    	Formation temp = ts.getFormation(formId);
    	if(temp==null)
    		return RestUtils.getNotFoundResponse();
    	
    	IndexingResponse ir = is.getRPNsWithFormations(Arrays.asList(temp), 0, 0);
    	if(ir==null)
    		return RestUtils.getNotFoundResponse();	
    	
    	JSONObject jsono = new JSONObject();
    	jsono.put("total",ir.totalDoc);
    	return RestUtils.getResponse(jsono.toString());
    }
    
  


    public boolean goodStartEnd(int start, int end){
    	if(start<0 || end-start>19 || end<start)
    		return false;
    	return true;
    }
    
    
    
}
