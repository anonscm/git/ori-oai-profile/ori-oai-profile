/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.ws.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;
import org.orioai.profile.FormationService;
import org.orioai.profile.entity.Formation;

/**
 * Cette classe represente une interface Rest du module
 * url /formation
 * @author kruczek
 *
 */
@Path("formation")
public class FormationWebInterface {
	FormationService tempS=FormationService.getInstance();

	public FormationWebInterface() {

	}
	
	public void setFormationService(FormationService fs) {
		tempS=fs;
	}
	
	/**
	 * Recupere la liste des formations du module /formation/all
	 * @return la lsite des formation au format JSON
	 */
	@GET
	@Path("all")
	@Produces({"application/json"+ ";charset=utf-8"})
    public Response getAllFormations(){
		List<Formation> temps = tempS.getFormations();
	
		JSONArray jArray = JSONFormat.getFormations(temps);
		return RestUtils.getResponse(jArray.toString());
    }
	
	/**
	 * Recupere la liste des formations a afficher sur la page d'index
	 * Requete appelee depuis le javascript du client
	 * @return la liste des formations
	 */
	@GET
	@Path("indexall")
	@Produces({"application/json"+ ";charset=utf-8"})
    public Response getAllIndexFormations(){
		List<ArrayList<String>> itemps = tempS.getIndexFormations();
		
		if(itemps.isEmpty())
			return RestUtils.getNotFoundResponse();	
	
		JSONArray jsono = JSONFormat.getListIdFormations(itemps);
		return RestUtils.getResponse(jsono.toString());
    }
	
	
	/**
	 * Recupere les informations d'une formation (id, nom, deweys, mots-cles).
	 * /formation/{id}
	 * @param id id de la formation
	 * @return les information au format JSON
	 */
	@GET
	@Path("{id}")
    public Response  getFormation(@PathParam("id") String id){
    	Formation temp = tempS.getFormation(id);
    	if(temp==null){
    		return RestUtils.getNotFoundResponse();
    	}

    	JSONObject formJSON =JSONFormat.getFormation(temp);
    	return RestUtils.getResponse(formJSON.toString());
   
    }
    
}
