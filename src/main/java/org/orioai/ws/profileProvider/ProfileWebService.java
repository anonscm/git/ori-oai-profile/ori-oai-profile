/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.ws.profileProvider;



import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import org.apache.log4j.Logger;
import org.orioai.profile.ContextListener;
import org.orioai.profile.ProfileService;
import org.orioai.profile.entity.Profile;
import org.orioai.profile.entity.XMLProfile;

/**
 * Interface principale du ws.
 * Methode appeles par le module ori-oai-search.
 * 
 * @author kruczek
 *
 */
@WebService
@SOAPBinding(style = Style.RPC)
public class ProfileWebService {

	public static ProfileService profileService;
	
    
    /**
     * Initialisation du ws
     */
    public static void setProfileService(ProfileService ps){
    	profileService = ps;
    }

   /**
    * Preparation du profile et mise en cache
    * @param uid
    */
   public void prepareProfile(String uid){
	   profileService.prepareProfile(uid);
   }
   
   /**
    * Recuparation du profile RPN d'un etudiant
    * @param uid uid de l'etudiant
    * @param typeMap indication du profile a retourner (non utilise actuellement) 
    * @return le profile 
    */
   public XMLProfile  getProfile(String uid, String typeMap){
	   Profile p = profileService.getProfile(uid);  
	   XMLProfile xp =  new XMLProfile(p);
	   return xp;
   }
   

}
