/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.orioai.profile.entity.Formation;
import org.orioai.profile.file.XMLParser;
import org.orioai.profile.file.explorer.FormationExplorer;
import org.orioai.ws.rest.RPNWebInterface;
import org.w3c.dom.Document;

/**
 * Main class for formations
 * @author kruczek
 *
 */
public class FormationService {
	private static Logger log = Logger.getLogger(FormationService.class);
	static FormationService instance;
	
	List<Formation> formations=null;
	List<ArrayList<String>> indexFormations=null;

	
	private FormationService (){
		log.debug("[FORMATION] initialization");
		init();
	}
	
	/**
	 * Recupere l'instance du service (singleton)
	 * @return
	 */
	public static synchronized FormationService getInstance() {
		if (instance == null) {
			instance = new FormationService();
		}
		return instance;
	}
	
	public void init(){
		getFormations();
		initFormationsIndex();
		RPNWebInterface.setFormationService(this);
	}
	
	public void initFormationsIndex(){
		List<ArrayList<String>> iTemps=new ArrayList<ArrayList<String>>();
		
		Document doc = XMLParser.getDocumentFromURL(Settings.XML_FORMATIONS_INDEX_URL);
		FormationExplorer tempExplore = new FormationExplorer(doc);
		iTemps = tempExplore.getIndexFormations();
		
		log.debug("[Formation] "
				+(iTemps.get(0).size()+iTemps.get(1).size()+iTemps.get(2).size())
				+" formations found for index page");
		this.indexFormations=iTemps;
	}
	
	public void setFormations(List<Formation> temps){
		this.formations = temps;
	}
	
	/**
	 * Recupere les formations
	 * @return la liste des formations
	 */
	public List<Formation> getFormations(){
		if(formations==null){
			//dataTemplates = getTemplatesFromFile();
			formations = getFormationsFromURL();
			log.debug("[Formations] "+formations.size()
			+" formations found");
		}
		return formations;
	}
	
	public List<ArrayList<String>> getIndexFormations(){
		return indexFormations;
	}
	
		
	
	/**
	 * Recupere les templates correspondants a une formation
	 * @param formation la formation
	 * @return les lsite des templates
	 */
	public List<Formation> getFormations(List<String> idFormations){
		List<Formation> temps=new ArrayList<Formation>();

		List<Formation> templates = getFormations();
		if (templates==null){
			return new ArrayList<>();
		}
			
		for(int i=0;i<templates.size();i++){
			Formation template =templates.get(i);
			
			if(idFormations.contains(template.getId())){
				temps.add(template);
			}
		}
		
		return temps;
	}
	
	public Formation getFormation(String idFormation){
		List<Formation> templates = getFormations();
		if (templates==null){
			return null;
		}
		
		for(int i=0;i<templates.size();i++){
			Formation template =templates.get(i);
			
			if(template.getId().equals(idFormation)){
				return template;
			}
		}
		return null;
	}
	
	/**
	 * Recupere la liste des templates a partir d'une url
	 * @return la liste des formations
	 */
	private List<Formation> getFormationsFromURL(){
		List<Formation> temps=new ArrayList<Formation>();
		Document doc = XMLParser.getDocumentFromURL(Settings.XML_FORMATIONS_URL);
		FormationExplorer tempExplore = new FormationExplorer(doc);
		temps = tempExplore.getFormations();
		return temps;
	}
	

	
}
