package org.orioai.profile;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.orioai.profile.file.XMLParser;
import org.orioai.profile.file.explorer.VocabularyLomTypeExplorer;
import org.w3c.dom.Document;

/**
 * 
 * @author kruczek
 *
 */
public class VocabularyService {
	private static Logger log = Logger.getLogger(VocabularyService.class);
	Map<String,String> lomFrTypeMap = new HashMap<String,String>();
	static VocabularyService vs=null;
	
	private VocabularyService() {
		init();
	}
	
	public static VocabularyService getInstance(){
		if(vs==null){
			vs=new VocabularyService();
		}
		return vs;
	}
	
	public void init(){
		initLOMTypes();
	}

	/**
	 * Initialisation des types de documents LOM
	 */
	public void initLOMTypes(){
		String url = Settings.VOCABULARY_LOM_TYPE_URL;
		if(url==null)
			return;
		
		Document doc = XMLParser.getDocumentFromURL(url);
		if(doc!=null){
			lomFrTypeMap = VocabularyLomTypeExplorer.getType(doc);
			log.debug("[Vocabulary] "+lomFrTypeMap.size()+" lom types found ");
		}
	}
	
	
	/**
	 * Traduit le type de document en francais
	 * @param enType le type en anglais
	 * @return le type en francais
	 */
	public String getFrType(String enType){
		return lomFrTypeMap.get(enType);
	}
	
	
}
