/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui represente une ressource pedagogique numérique.
 * @author kruczek
 *
 */
public class RPN {

    public String id;
    public String idUNT;
    public String title;
    public String url;
    public List<String> authors ;
    public String date;
    public String description;
    public List<String> types;
    public String urlPicture;
    
    public RPN(String id){
    	authors=new ArrayList<String>();
    	types=new ArrayList<String>();
    	this.id=id;
    }
    
    public String toString(){
    	return "{\nid:"+id+",\nidUNT:"+idUNT+",\ntitle:"+title+",auteurs:"+authors+"}";
    }

}
