/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile.entity;

import java.util.List;

/**
 * Classe qui represente une reponse de l'indexing 
 * avec la liste de RPN retourne et le nombre total de RPN en BDD
 * @author kruczek
 *
 */
public class IndexingResponse {
	
	public long totalDoc;
	public List<RPN> rpns;

	public IndexingResponse(long totalDoc, List<RPN> rpns) {
		this.totalDoc=totalDoc;
		this.rpns=rpns;
	}
}
