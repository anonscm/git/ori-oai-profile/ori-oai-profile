/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile.entity;

import java.util.ArrayList;
import java.util.List;



/**
 * Classe representant l'association entre une formation (ID, nom) 
 * et des metadonnées de ressource (code deweys, mots-cles)
 *
 * @author kruczek
 *
 */
public class Formation {
	//pair balise value
	String id;
	String name;
	List<String> deweys;
	List<String> keywords;
	List<String>  green = new ArrayList<String>();
	List<String>  red = new ArrayList<String>();
	
	public Formation(){
		deweys = new ArrayList<String>();
		keywords = new ArrayList<String>();

	}
	
	public Formation(String id, String name){
		this.id=id;
		this.name=name;
		deweys = new ArrayList<String>();
		keywords = new ArrayList<String>();
	}
	
	
	public Formation(String id, String name,List<String> deweys,List<String> keywords){
		this.id=id;
		this.name=name;
		this.deweys=deweys;
		this.keywords=keywords;
	}

	
	public void setId(String id) {
		this.id = id;
	}
	public String getId() {
		return this.id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}

	public List<String> getDeweys() {
		return deweys;
	}

	public void setDeweys(List<String> deweys) {
		this.deweys = deweys;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}
	public List<String> getGreen() {
		return green;
	}

	
	public void setGreen(List<String> green){
		this.green=green;
	}
	
	public List<String> getRed() {
		return red;
	}
	public void setRed(List<String> red){
		this.red=red;
	}
	
	public String toString(){
		String s="Template={";
		s=s+id+";";
		s=s+name+";";
		s=s+deweys+";";
		s=s+keywords;
		/*for(Id id : formation.getIds()) {
		   s=s+ id.name+":"+id.value+",";
		    
		}*/
		s=s+'}';
		return s;
	}
	
	


}
