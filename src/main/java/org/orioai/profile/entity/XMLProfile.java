/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile.entity;


import java.util.Arrays;

import javax.xml.bind.annotation.XmlRootElement;

import org.orioai.profile.Settings;

/**
 * Classe qui represente un profile en xml :
 * 
 * <strings>
 * ...
 * </strings>
 * 
 * @author kruczek
 *
 */
@XmlRootElement
public class XMLProfile {
	
	//nom parametre = nom balise xml
	public XMLProperties strings;
	
	public XMLProfile() {
	}

	public XMLProfile(XMLProperties profile) {
		this.strings=profile;
	}
	
	public XMLProfile(Profile profile) {
		strings=  new XMLProperties();
		
		XMLProperty prop = new XMLProperty(Settings.SOAP_XML_ID_UID, Arrays.asList(profile.getUid()) );
		strings.add(prop);
		
		if(profile.getDisplayName() != null){
			XMLProperty prop7 = new XMLProperty(Settings.SOAP_XML_ID_DISPLAYNAME,Arrays.asList(profile.getDisplayName()));
			strings.add(prop7);
		}
		

		
		if(profile.getIdFormation() != null){
			XMLProperty prop6 = new XMLProperty(Settings.SOAP_XML_ID_FORM,profile.getIdFormation());
			strings.add(prop6);
		}
		
		if(profile.getDeweys() != null){
			XMLProperty prop2 = new XMLProperty(Settings.SOAP_XML_ID_DEWEYS,profile.getDeweys());
			strings.add(prop2);
		}
		
		if(profile.getKeywords() != null){
			XMLProperty prop3 = new XMLProperty(Settings.SOAP_XML_ID_KEYWORDS,profile.getKeywords());
			strings.add(prop3);
		}
		
		if(profile.getGreen() != null){
			XMLProperty prop4 = new XMLProperty(Settings.SOAP_XML_ID_GREEN,profile.getGreen());
			strings.add(prop4);
		}
		
		if(profile.getRed() != null){
			XMLProperty prop5 = new XMLProperty(Settings.SOAP_XML_ID_RED,profile.getRed());
			strings.add(prop5);
		}
		
		
	}
	
	


}
