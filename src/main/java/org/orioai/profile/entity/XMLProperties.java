/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile.entity;

import java.util.ArrayList;
import java.util.List;


/**
 * Classe qui represente une liste de propiete en xml :
 * 
 *  <stringList>
 *  ..
 *  </stringList>
 *  <stringList>
 *  ...
 *  </stringList>
 *  
 * @author kruczek
 *
 */
public class XMLProperties {

	//nom parametre = nom balise xml
	public List<XMLProperty> stringList=new ArrayList<XMLProperty>();
	
	public XMLProperties() {
		// TODO Auto-generated constructor stub
	}

	public XMLProperties(List<XMLProperty> properties) {
		this.stringList=properties;
	}
	
	public void add(XMLProperty prop){
		stringList.add(prop);
	}
	

}

