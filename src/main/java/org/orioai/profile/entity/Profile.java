package org.orioai.profile.entity;
/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/


import java.util.ArrayList;
import java.util.List;

/**
 * Represente le profil rpn d'un etudiant 
 * avec son uid 
 * ses codes dewey/mots-clefs associés
 * et ses listes rouge/verte
 * 
 * Il est constitue a partir d'1 ou de plusieurs templates
 * 
 * @author kruczek
 *
 */
public class Profile {
	private String uid;
	private String displayName=null;

	List<Formation> formations = new ArrayList<Formation>();
	
	public Profile(String uid) {
		this.uid= uid;
	}
	
	public void setDisplayName(String displayName){
		this.displayName=displayName;
	}

	
	
	public String getUid(){
		return uid;
	}
	
	public void addFormations(List<Formation> forms){
		this.formations.addAll(forms);
	}

	
	public List<String> getDeweys(){
		List<String> deweys = new ArrayList<String>();
		for(Formation form : formations){
			deweys.addAll(form.getDeweys());
		}
		return deweys;
	}
	
	public List<String> getKeywords(){
		List<String> words = new ArrayList<String>();
		for(Formation form : formations){
			words.addAll(form.getKeywords());
		}
		return words;
	}
	
	public List<String> getGreen(){
		List<String> greens = new ArrayList<String>();
		for(Formation form : formations){
			greens.addAll(form.getGreen());
		}
		return greens;
	}
	
	public List<String> getRed(){
		List<String> reds = new ArrayList<String>();
		for(Formation form : formations){
			reds.addAll(form.getRed());
		}
		return reds;
	}
	
	public List<String> getIdFormation(){
		List<String> ids = new ArrayList<String>();
		for(Formation form : formations){
			ids.add(form.getId());
		}
		return ids;
	}
	
	public  String getDisplayName(){
		return displayName;
	}	

	
	
	public String toString(){
		return uid+","+getDeweys()+";"+getKeywords();
	}

}
