/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.orioai.profile.entity.Formation;
import org.orioai.profile.entity.Profile;
import org.orioai.profile.file.PropertiesFileParser;
import org.orioai.profile.ldap.LDAPService;
import org.orioai.ws.profileProvider.ProfileWebService;

/**
 * Main class for app.
 * Do the link between :
 * - receipt soap request from Ori-oai-Search, 
 * - ldap interrogation,
 * - and formations list (mapping idformation/dewey code/keyword)
 * 
 * @author kruczek
 *
 */
public class ProfileService {
	private static Logger log = Logger.getLogger(ProfileService.class);

	private static ProfileService instance;
	public FormationService ts;
	public LDAPService ldapS;

	
	private ProfileService(){
		log.debug("[Profile] initialization");
		init();
	}

	/**
	 * Recupere l'instance de ProfileService (singleton)
	 * @return
	 */
	public static synchronized ProfileService getInstance() {
		if (instance == null) {
			instance = new ProfileService();
		}
		return instance;
	}
	
	public void init(){
		initConfig();
		ProfileWebService.setProfileService(this);
		ldapS = LDAPService.getInstance();
		ts = FormationService.getInstance();
		VocabularyService.getInstance();
	}
	
	
	public void initConfig(){	
		PropertiesFileParser  pfp = new PropertiesFileParser();
		Properties props = pfp.getProperties(Settings.PATH_CONFIG);

		Settings.LDAP_ID_FORMATION= props.getProperty("LDAP_ID_FORMATION");
		Settings.INDEXING_URL = props.getProperty("INDEXING_URL");
		Settings.THUMBNAIL_URL = props.getProperty("THUMBNAIL_URL");
		Settings.LDAP_DISPLAYNAME=  props.getProperty("LDAP_ID_DISPLAYNAME");
		Settings.VOCABULARY_LOM_TYPE_URL= props.getProperty("VOCABULARY_LOM_TYPE_URL");
		Settings.XML_FORMATIONS_URL= props.getProperty("XML_FORMATIONS_URL");
		Settings.XML_FORMATIONS_INDEX_URL= props.getProperty("XML_FORMATIONS_INDEX_URL");
	}
	
	/**
	 * Preparation du profil
	 * @param uid uid de l'etudiant
	 */
	public void prepareProfile(String uid){		
	}
	
	/**
	 * Creation du profil de l'etudiant
	 * @param uid uid de l'etudiant
	 * @return le profil
	 */
	public Profile createProfile(String uid){
		Profile profile = new Profile(uid);
		Map<String,List<String>> ldapInfos = ldapS.getLDAPValues(uid);
		
		//name
		List<String> names = ldapInfos.get(Settings.LDAP_DISPLAYNAME);
		if(names.size()>0)
			profile.setDisplayName(names.get(0));
		
		//id formations
		List<String> idFormations = ldapInfos.get(Settings.LDAP_ID_FORMATION);
		log.debug("[PROFILE] "+idFormations.size()
		+" formation ID in ldap server for '"+uid+"'");

		//formations
		List<Formation> templates = ts.getFormations(idFormations);
		log.debug("[PROFILE] "+templates.size()+
				" formations in module for '"+uid+"'\n");
		profile.addFormations(templates);

		return profile;
	}
	
	/**
	 * Recuperation d'un profil en cache ou creation
	 * @param uid uid de l'etudiant
	 * @return le profil
	 */
	public Profile getProfile(String uid){
		Profile profile = this.createProfile(uid);
		return profile;
	}
	
	/**
	 * Sauvegarde d'un profil en cache
	 */
	public void saveProfile(){
		
	}

}
