/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile.indexing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.orioai.indexing.query.LuceneQueryGenerator;



/**
 * Classe qui permet de construire les requete pour l'indexing
 * 
 * Structure de la requete :
 * ( <deweys> OR <keywords> OR <greens> ) AND NOT <reds> AND NOT <exam> 
 * 
 * GREENS / REDS
 * - md-ori-oai-id
 * 
 * DEWEYS
 * - lom.classification.ddc.id
 * - onixdc.Product.Subject.SubjectCode
 * 
 * KEYWORDS
 * - lom.general.title_fr
 * - lom.general.title
 * - onixdc.title
 * - lom.general.description_fr
 * - lom.general.description
 * - lom.general.keyword_fr
 * - onixdc.subject_fr
 * - 
 * 
 * EXAM
 * - lom.lomlille1.typeDoc
 * 
 * @author kruczek
 *
 */
public class IndexingQueryFormat {
	static String mdId = 			"md-ori-oai-id";
	static String lomTitle = 		"lom.general.title_fr";
	static String onixTitle = 		"onixdc.title";
	static String lomDesc = 		"lom.general.description_fr";
	static String lomKeys = 		"lom.general.keyword_fr";
	static String onixKeys = 		"onixdc.subject_fr";
	static String lomDeweys = 		"lom.classification.ddc.id";
	static String onixDeweys = 		"onixdc.Product.Subject.SubjectCode";
	static String lomLilleType = 	"lom.lomlille1.typeDoc";
	static LuceneQueryGenerator qg = new LuceneQueryGenerator();
	
	/**
	 * Requete sur le titre des RPN
	 * @param titles les mots a rechercher
	 * @return la requete
	 */
	private static String getTitleQuery(String titles){
		String lom = qg.getValueAssociation(lomTitle, titles,false);
		String onix = qg.getValueAssociation(onixTitle, titles,false);
		return qg.getAssociation(Arrays.asList(lom,onix), LuceneQueryGenerator.OR,false);		
	}
	

	/**
	 * Requete sur l'id des RPN
	 * @param ids IDs a rechercher
	 * @return la requete
	 */
	private static String getIdQuery(String ids){
		return  qg.getValueAssociation(mdId, ids,false);
	}
	
	/**
	 * Requete sur la description des RPN
	 * @param words les mots a rechercher
	 * @return la requete
	 */
	private static String getQueryDesc(String words){
		return qg.getValueAssociation(lomDesc, words,false);
	}
	
	/**
	 * Requete sur les mots-cles des RPN
	 * @param words les mots-clefs a rechercher
	 * @return la requete
	 */
	private static String getQueryKeywords(String words){
		String lom = qg.getValueAssociation(lomKeys, words,false);
		String onix = qg.getValueAssociation(onixKeys, words,false);
		return qg.getAssociation(Arrays.asList(lom,onix), LuceneQueryGenerator.OR,false);
	}
	
	/**
	 * Requete sur les code deweys des RPN
	 * @param deweys les deweys a rechercher
	 * @return la requete
	 */
	private static String getQueryDeweys(String deweys){
		String lom = qg.getValueAssociation(lomDeweys, deweys,false);
		String onix = qg.getValueAssociation(onixDeweys, deweys,false);
		return qg.getAssociation(Arrays.asList(lom,onix), LuceneQueryGenerator.OR,false);
	}
	
	
	
	/**
	 * Requete pour recuperer les sujets d'examen
	 * @return la requete
	 */
	private static String getQueryExam(){
		return qg.getValueAssociation(lomLilleType, "suj_exam",false);
		
	}
	
	/**
	 * Requete qui recherche dans les champs textuels des RPN
	 * <title> OR <description> OR <keywords>
	 * @param words les mots a rechercher
	 * @return la requete
	 */
	private static String getWordsQuery(String words){	
		String titlesq = getTitleQuery(words);
		String descq = getQueryDesc(words);
		String keysq  = getQueryKeywords(words);
		
		String query= qg.getAssociation(Arrays.asList(titlesq,descq,keysq),LuceneQueryGenerator.OR,false);
		return query;
	}
	
	
	

	/**
	 * Requete complete pour envoyer sur l'indexing
	 * ( <deweys> OR <words> OR <green> ) AND NOT <reds> AND NOT <exam> 
	 * 
	 * @param deweys liste des codes deweys a chercher
	 * @param keywords liste des mots cles a chercher
	 * @param greens liste des id a chercher
	 * @param reds liste des id a exclure
	 * @return la requete complete 
	 */
	public static String getQuery(
			List<String> deweys, List<String> keywords,
			List<String> greens, List<String> reds){	
		

		String orq = getDeweysKeywordsGreensQueryPart(deweys, keywords, greens);
		String redq= qg.getNotRequest(getIdQuery(getOrQuery(cleanIDs(reds))));
		String examq = qg.getNotRequest(getQueryExam());
	
		String query = getAndQuery(Arrays.asList(orq,redq,examq));
		return query;
		
	}
	
	/**
	 * Requete qui recherche des rpn a partir de code deweys, mots cles et id
	 * <deweys> OR <words> OR <green>
	 * @param deweys les code deweys
	 * @param keywords les mots cles
	 * @param greens les ids 
	 * @return la requete
	 */
	private static String getDeweysKeywordsGreensQueryPart(
			List<String> deweys,
			List<String> keywords,
			List<String> greens){
			
		String deweysq = getQueryDeweys( getOrQuery(cleanDeweys(deweys)) );
		String wordsq = getWordsQuery( getOrQuery(cleanWords(keywords)) );	
		String greenq = getIdQuery( getOrQuery(cleanIDs(greens)) );	
		
		String query = qg.getAssociation(Arrays.asList(deweysq,wordsq,greenq), LuceneQueryGenerator.OR);
		return query;
	}
	
	
	
	
	
	/**
	 * Formater une liste de mots pour les requetes
	 * @param strs les mots 
	 * @return les mots formater
	 */
	private static String getOrQuery(List<String> strs){
		return qg.getAssociation(strs,LuceneQueryGenerator.OR);
	}
	
	private static String getAndQuery(List<String> strs){
		return qg.getAssociation(strs,LuceneQueryGenerator.AND);
	}
	
	
	
	
	/**
	 * Nettoyer un mot pour les requetes
	 * Enleve accent ?
	 * @param word le mot a nettoyer
	 * @return le mot nettoye
	 */
	private  static String cleanWord(String word){
		return word.replace(" ", "_")
				.replace("é", "e")
				.replace("è", "e")
				.replace("É", "e")
				;
		
	}
	/**
	 * Nettoyer une liste de mots
	 * @param words les mots
	 * @return les mots nettoyes
	 */
	private static List<String> cleanWords(List<String> words){
		List<String> cleanWords = new ArrayList<String>();
		if(words==null)
			return cleanWords;
		for(int i=0;i<words.size();i++){
			cleanWords.add(cleanWord(words.get(i)));
		}
		return cleanWords;
	}
	
	/**
	 * Nettoyer un code deweys pour les requetes
	 * @param dewey le code deweys
	 * @return le code deweys nettoye
	 */
	private  static String cleanDewey(String dewey){
		return dewey.replace(" ","?");
	}
	
	/**
	 * Nettoyer des codes deweys pour les requetes
	 * @param deweys les codes deweys
	 * @return les code deweys nettoyes
	 */
	private static List<String> cleanDeweys(List<String> deweys){
		List<String> cleanDeweys = new ArrayList<String>();
		if(deweys==null)
			return cleanDeweys;
		for(int i=0;i<deweys.size();i++){
			cleanDeweys.add(cleanDewey(deweys.get(i)));
		}
		return cleanDeweys;
	}
	
	private static String cleanID(String id){
		return "\""+id+"\"";
	}
	
	private static List<String> cleanIDs(List<String> ids){
		List<String> cleanIDs = new ArrayList<String>();
		if(ids==null)
			return cleanIDs;
		for(int i=0;i<ids.size();i++){
			cleanIDs.add(cleanID(ids.get(i)));
		}
		return cleanIDs;
	}
}
