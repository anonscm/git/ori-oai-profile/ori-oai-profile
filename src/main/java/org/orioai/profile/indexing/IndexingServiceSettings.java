package org.orioai.profile.indexing;


public class IndexingServiceSettings {
	public static String INDEXING_CLIENT_NAME = "indexingClient";
	
	/* for resources in LOM format*/
	public static String NAMESPACE_LOM="http://ltsc.ieee.org/xsd/LOM";
	public static String PATH_LOM_TITLE="//title/string";
	public static String PATH_LOM_IDENTIFIER="//general/identifier/entry";
	public static String PATH_LOM_DESCRIPTION="//description/string";
	public static String PATH_LOM_LOCATION="//technical/location";
	public static String PATH_LOM_DATE="//lifeCycle/contribute/role/value[text()='author']/../../date/dateTime";
	public static String PATH_LOM_AUTHORS="//lifeCycle/contribute/role/value[text()='author']/../../entity";
	public static String PATH_LOM_AUTHORS_SFA="//lifeCycle/contribute/role/value[text()='author']/../../entity/addressBook/vcard/type[@name='FN']/value/text";
	public static String PATH_LOM_TYPES="//educational/learningResourceType/value";
	
	/* for resources in ONIX DC format (ebook)*/
	public static String NAMESPACE_ONIXDC = "http://bibnum.bnf.fr/NS/onix_dc/";
	public static String PATH_ONIXDC_TITLE="/dc/title";
	public static String PATH_ONIXDC_IDENTIFIER="/dc/identifier";
	public static String PATH_ONIXDC_URL="//link/target_url";
	public static String PATH_ONIXDC_DATE="/dc/date";
	public static String PATH_ONIXDC_AUTHORS="/dc/creator";
	public static String PATH_ONIXDC_PICTURE="//link/picture_url";
	public static String ONIXDC_TYPE = "livre numérique";
		
}
