/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile.indexing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.orioai.profile.entity.Formation;
import org.orioai.profile.entity.IndexingResponse;
import org.orioai.profile.entity.RPN;
/**
 * Service permettant de communiquer avec l'indexing de Ori-oai
 * @author kruczek
 *
 */
public class IndexingService {

	IndexingClient ic=null;
	
	public IndexingService() {
		ic = new IndexingClient();
	}
	
	public IndexingService(IndexingClient ic) {
		this.ic=ic;
	}

	/**
	 * Recupere une RPN avec son ID
	 * @param id de la RPN
	 * @return la reponse de l'indexing
	 */
	public RPN getRPNWithId(String id){	
		if(id==null)
			return null;
		
		String query = IndexingQueryFormat.getQuery(null, null, Arrays.asList(id), null);
		IndexingResponse indexResp = ic.searchXMLDocs(query, 1, 1);
		if(indexResp.rpns.size()==0)
			return null;
		
		RPN rpn=indexResp.rpns.get(0);
		return rpn;
	}
	
	/**
	 * Recherche une RPN en fonction de mots
	 * @param kwords les mots a rechercher
	 * @param start indice de la premiere rpn de la reponse
	 * @param end indice de la derniere rpn de la reponse
	 * @return la reponse de l'indexing ou null
	 */
	public IndexingResponse getRPNsWithWords(List<String> kwords,int start, int end){
		if(kwords==null)
			return null;
		
		String query = IndexingQueryFormat.getQuery(null, kwords, null, null);
		IndexingResponse indexResp  = ic.searchFromAttributes(query, start, end);		
		return indexResp;
	}
	
	
	/**
	 * Recherche les rpn des templates
	 * @param temps les templates
	 * @param start indice de la premiere rpn de la reponse
	 * @param end ndice de la derniere rpn de la reponse
	 * @return la reponse de l'indexing
	 */	
	public IndexingResponse getRPNsWithFormations(List<Formation> temps, int start, int end){
		List<String> deweys = new ArrayList<String>();
		List<String> keyws = new ArrayList<String>();
		List<String> reds = new ArrayList<String>();
		List<String> greens = new ArrayList<String>();
		
		for(int i=0;i<temps.size();i++){
			deweys.addAll(temps.get(i).getDeweys());
			keyws.addAll(temps.get(i).getKeywords());
			reds.addAll(temps.get(i).getRed());
			greens.addAll(temps.get(i).getGreen());
		}
		
		if(deweys.size()==0 && keyws.size()==0){
			return new IndexingResponse(0, new ArrayList<RPN>());
		}
		
		String query = IndexingQueryFormat.getQuery(deweys,keyws,greens,reds);
		IndexingResponse indexResp = ic.searchFromAttributes(query, start, end);
		return indexResp;
	}
	
	
	

}
