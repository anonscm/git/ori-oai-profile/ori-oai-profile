package org.orioai.profile.indexing;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.orioai.profile.Settings;
import org.orioai.profile.entity.IndexingResponse;
import org.orioai.profile.entity.RPN;
import org.orioai.profile.file.explorer.IndexingResponseExplorer;
import org.orioai.ws.indexing.OriOaiIndexingServiceInterface;
import org.orioai.ws.indexing.SearchResults;
import org.orioai.ws.indexing.exceptions.ISSearchException;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Classe permettant d'envoye une request soap a l'indexing
 * @author kruczek
 *
 */
public class IndexingClient {
	private static Logger log = Logger.getLogger(IndexingClient.class);
    OriOaiIndexingServiceInterface client;
    
    
    public IndexingClient() {
    	ClassPathXmlApplicationContext context = 
    			new ClassPathXmlApplicationContext(Settings.PATH_INDEXING_BEANS);
    	client = (OriOaiIndexingServiceInterface) context.getBean(
    			IndexingServiceSettings.INDEXING_CLIENT_NAME);
 
    }
    
    public IndexingClient(OriOaiIndexingServiceInterface client) {
    	this.client=client;
    }
    
   
    /**
     * Envoie une requete sur la methode SFA de l'indexing
     * Avec les parametres pour trier par dates decroissantes
     * 
     * @param query requete pour trouver des documents
     * @param start indice du premier doc de la reponse
     * @param end indice du dernier doc de la reponse
     * @return la reponse de l'indexing
     */
	public IndexingResponse searchFromAttributes(String query, int start, int end){
		String dateAttr = "date_creation_sort";
		long nbResults=0;
		SearchResults sr=null;
		
		try {
			sr = client.searchFromAttributes(query, null, null, Arrays.asList(dateAttr), start, end, Arrays.asList(dateAttr), Arrays.asList(false), null);
			nbResults = sr.getNumberOfResults();
		} catch (ISSearchException e) {
			log.error("[Indexing] Error when request searchFromAttributes method" );
			log.error(e);
		}
		
		IndexingResponseExplorer ire = new IndexingResponseExplorer(sr);
		List<RPN> rpns = ire.getRPNList();
		IndexingResponse ir = new IndexingResponse(nbResults, rpns);
		return ir;
	}
	
    /**
     * Envoie une requete sur la methode searchXMLDoc2 de l'indexing
     * @param query requete pour trouver des documents
     * @param start indice du premier doc de la reponse
     * @param end indice du dernier doc de la reponse
     * @return la reponse de l'indexing
     */
	public IndexingResponse searchXMLDocs(String query, int start, int end){
		long nbResults=0;
		SearchResults sr=null;
		
		try {
			sr = client.searchXMLDocs(query, null, start, end);
			nbResults = sr.getNumberOfResults();
		} catch (ISSearchException e) {
			log.error("[Indexing] Error when request searchXMLDocs method" );
			log.error(e);
		}
		
		IndexingResponseExplorer ire = new IndexingResponseExplorer(sr);
		List<RPN> rpns = ire.getRPNList();
		IndexingResponse ir = new IndexingResponse(nbResults, rpns);
		return ir;
	}
	
}
