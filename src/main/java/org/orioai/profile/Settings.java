package org.orioai.profile;
/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/

/**
 * Classe de configuration du WS
 * 
 * @author kruczek
 *
 */
public class Settings {
	private Settings() {

	}
	
	public static String INDEXING_URL = null;
	public static String THUMBNAIL_URL = null;
	public static String VOCABULARY_LOM_TYPE_URL=null;
	public static String XML_FORMATIONS_URL=null;
	public static String XML_FORMATIONS_INDEX_URL=null;
	
	public static String LDAP_ID_FORMATION=null;
	public static String LDAP_DISPLAYNAME=null;
	
	public static String PATH_CONFIG="/config.properties";
	public static String PATH_INDEXING_BEANS = "/indexing-beans.xml";
	
	/*XML tags of formations*/
	public static String TAG_FORMATION = "formation";
	public static String XML_FORMATION_ID = "id";
	public static String XML_FORMATION_NAME = "name";
	public static String XML_FORMATION_DEWEY = "dewey";
	public static String XML_FORMATION_KEYWORD = "keyword";
	public static String XML_FORMATION_GREEN = "green";
	public static String XML_FORMATION_RED = "red";
	public static String XML_FORMATION_RPN = "rpn";
	public static String XML_FORMATION_GROUP = "group";
	
	/* XML tags for SOAP response */
	public static String SOAP_XML_ID_UID = "uid";
	public static String SOAP_XML_ID_DISPLAYNAME= "name";
	public static String SOAP_XML_ID_FORM = "formations";
	public static String SOAP_XML_ID_DEWEYS = "deweys";
	public static String SOAP_XML_ID_KEYWORDS= "keywords";
	public static String SOAP_XML_ID_GREEN= "green";
	public static String SOAP_XML_ID_RED= "red";
	
	/*XML tags of lom types*/
	public static String VOC_LOMTYPE_ITEM_TAG = "vdex:term";
	public static String VOC_LOMTYPE_ID_TAG = "vdex:termIdentifier";
	public static String VOC_LOMTYPE_LANG_TAG = "vdex:langstring";
	public static String VOC_LOMTYPE_LANG_ATTR = "language";
	public static String VOC_LOMTYPE_FR_VALUE = "fr";

}
