package org.orioai.profile;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;




/**
 * Class for init app when start
 * @author kruczek
 *
 */
public class ContextListener implements ServletContextListener {

	private static Logger log = Logger.getLogger(ContextListener.class);
	
	
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		log.debug("[START] Initialization");
		ProfileService.getInstance().init();
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		
	}

}