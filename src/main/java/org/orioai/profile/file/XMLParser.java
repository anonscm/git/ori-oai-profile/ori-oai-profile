/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile.file;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.orioai.profile.ContextListener;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Classe qui parse des sources xml en objet Document
 * @author kruczek
 *
 */
public class XMLParser {
	private static Logger log = Logger.getLogger(XMLParser.class);
	
	//XPathFactory xpf;
	static XPath path = XPathFactory.newInstance().newXPath();
	
	
	/**
	 * Retourne un objet Document a partir du chemin d'un fichier xml
	 * @param path chemin du fichier xml
	 * @return l'objet java representant la structure xml, NULL sinon
	 */
	public  Document getDocumentFromFile(String path) { 
		Document document=null;
		
		InputStream input = this.getClass().getResourceAsStream(path);
		if(input==null)
			return document;
		
		try {
			final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();	
			document = builder.parse(input);
			
		} catch (SAXException | IOException | ParserConfigurationException e) {
			log.error("[XML] Parsing error for file "+path);
			log.error(e);
		}

		return document;
	}

	/**
	 * Retourne un objet Document a partir d'un chaine 
	 * @param xml la chaine au format xml
	 * @return l'objet java representant la structure xml, NULL sinon
	 */
	public static Document getDocumentFromString(String xml){
		Document document =null;
		
		if(xml==null)
			return document;
		
		try {	
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 	
			DocumentBuilder builder = factory.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(xml));
			document = builder.parse(is);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			log.error("[XML] Parsing error");
			log.error(e);
		}

		return document;
	}
	
	
	public static Document getDocumentFromURL(String url){
		Document document =null;
		try {	
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 	
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(new URL(url).openStream());
		} catch (SAXException | IOException | ParserConfigurationException e) {
			log.error("[XML] Parsing error for url : "+url);
			log.error(e);
		}

		return document;
		
	}
	
	/**
	 * Recupere la valeur d'une balise xml 
	 * @param expression l'expression XPATH permettant de retrouver la balise
	 * @param obj l'objet (element, node) contenant la balise XML
	 * @return la valeur de la balise sous forme de chaine, null sinon
	 */
	public static String getString(String expression, Object obj ){
		String str=null;
		
		try {
			str = (String)path.evaluate(expression, obj);
			str=str.trim();
		} catch (XPathExpressionException e) {
			//log.error(e);
		}
		
		return str;
	}
	
	public static List<String> getStringList(String exp, Object obj){
		List<String> list = new ArrayList<String>();
		NodeList nlist = getNodeList(exp,obj);
		for(int i=0;i<nlist.getLength();i++){
			list.add(nlist.item(i).getTextContent());
		}
		return list;
	}
	
	/**
	 * Recupere une partie d'une structure xml sous forme de NodeList
	 * @param expression l'expression xpath permettant de retrouver la partie de la structure
	 * @param obj la structrue xml (element, node)
	 * @return la liste de Node, null sinon
	 */
	public static NodeList getNodeList(String expression, Object obj){
		NodeList nodeList=null;
	
		try {
			nodeList = (NodeList)path.evaluate(expression,obj, XPathConstants.NODESET);
		} catch (XPathExpressionException e) {
			//log.error(e);
		}

		return nodeList;
	}
	
	

	

}
