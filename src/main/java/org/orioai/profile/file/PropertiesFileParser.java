/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile.file;


import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Classe permettant la lecture des fichier ".properties"
 * 
 * @author kruczek
 *
 */
public class PropertiesFileParser {
	private static Logger log = Logger.getLogger(PropertiesFileParser.class);
	
	/**
	 * Retourne les proprietes trouves dans le fichier
	 * @param filePath le chemin du fichier de proprietes
	 * @return les proprietes, null sinon
	 */
	public Properties getProperties(String filePath){
		Properties prop = new Properties();

		InputStream input = this.getClass().getResourceAsStream(filePath);
		if(input==null){
			log.error("[FILE] properties file '"+filePath+"' not found");
			return prop;
		}
		
		try {
			prop.load(input);
		} catch (IOException e) {
			log.error("[FILE] loading error for properties file "+filePath);
			return null;
		}
		return prop;
	}
	
	
}
