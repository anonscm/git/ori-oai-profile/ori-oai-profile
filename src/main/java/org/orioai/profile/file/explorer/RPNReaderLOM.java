package org.orioai.profile.file.explorer;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.orioai.commons.exception.VcardException;
import org.orioai.commons.vcard.VcardService;
import org.orioai.profile.Settings;
import org.orioai.profile.VocabularyService;
import org.orioai.profile.file.XMLParser;
import org.orioai.profile.indexing.IndexingServiceSettings;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Classe pour recuperer des informations dans du XML LOM
 * @author kruczek
 *
 */
public class RPNReaderLOM implements RPNReader {
	Element root;
	private static Logger log = Logger.getLogger(RPNReaderLOM.class);
	
	public RPNReaderLOM(Element root) {
		this.root=root;
	}

	@Override
	public String getId() {
		return  XMLParser.getString(IndexingServiceSettings.PATH_LOM_IDENTIFIER, root);
	}

	@Override
	public String getTitle() {
		return XMLParser.getString(IndexingServiceSettings.PATH_LOM_TITLE, root);
	}

	@Override
	public String getDescription() {
		return XMLParser.getString(IndexingServiceSettings.PATH_LOM_DESCRIPTION, root);
	}

	@Override
	public String getURL() {
		return XMLParser.getString(IndexingServiceSettings.PATH_LOM_LOCATION, root);
	}

	@Override
	public String getDate() {
		return XMLParser.getString(IndexingServiceSettings.PATH_LOM_DATE, root);
	}

	@Override
	public List<String> getAuthors() {
		VcardService vcards= new VcardService();
		List<String> authors= new ArrayList<String>();
		
		authors=XMLParser.getStringList(
				IndexingServiceSettings.PATH_LOM_AUTHORS_SFA, root);
		if(authors.size()!=0)
			return authors;
		
		
		NodeList nodesAuthors =XMLParser.getNodeList(IndexingServiceSettings.PATH_LOM_AUTHORS, root);	
		for(int i=0;i<nodesAuthors.getLength();i++){
			//System.out.println(nodesAuthors.item(i).getTextContent())		
			String vcard = nodesAuthors.item(i).getTextContent();
			try {
				List<String> fn = vcards.extractAttributeValue(vcard, "FN");
				if(fn!=null && fn.size()>0)
					authors.add( fn.get(0).split(";")[0] );
			} catch (DOMException | VcardException e) {
				log.error("[Indexing] Error when extract author name in VCard : "+vcard);
				log.error(e);
			}	
		}
		
		
		return authors;	
	}
	
	

	@Override
	public List<String> getTypes() {
		List<String> types= new ArrayList<String>();
		
		NodeList nodesTypes = XMLParser.getNodeList(IndexingServiceSettings.PATH_LOM_TYPES, root);
		for(int i=0;i<nodesTypes.getLength();i++){
			String enType = nodesTypes.item(i).getTextContent();
			String type = VocabularyService.getInstance().getFrType(enType);
			types.add(type);
		}
		
		return types;
	}
	
	public String getURLPicture(){
		return Settings.THUMBNAIL_URL+"/get-img.do?url="+this.getURL()+"&size=360";
	}

}
