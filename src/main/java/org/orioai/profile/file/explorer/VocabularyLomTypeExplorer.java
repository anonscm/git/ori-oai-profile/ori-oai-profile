package org.orioai.profile.file.explorer;

import java.util.HashMap;
import java.util.Map;

import org.orioai.profile.Settings;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class VocabularyLomTypeExplorer {
	
	public static Map<String,String> getType(Document doc){
		Map<String,String> map = new HashMap<String,String>();
		
		NodeList nodes = doc.getElementsByTagName(Settings.VOC_LOMTYPE_ITEM_TAG);

		 for(int i=0;i<nodes.getLength();i++){
			 Element elem = (Element) nodes.item(i);
				
			 String typeEN = getTagValue(elem, Settings.VOC_LOMTYPE_ID_TAG);
		
			 NodeList nodesLang= elem.getElementsByTagName(Settings.VOC_LOMTYPE_LANG_TAG);
			 String typeFR = null;
			 for(int j=0;j<nodesLang.getLength();j++){
				 String lang= getAttributeValue(nodesLang.item(j), Settings.VOC_LOMTYPE_LANG_ATTR);
				 if(lang.equals(Settings.VOC_LOMTYPE_FR_VALUE))
					 typeFR=nodesLang.item(j).getTextContent();
			 }
	
			 map.put(typeEN, typeFR);
		 }

		return map;
		
	}
	
	public static String getTagValue(Element elem,String tag){
		 NodeList nodes = elem.getElementsByTagName(tag);
		 if(nodes.getLength()==0)
			 return null;
		 return nodes.item(0).getTextContent();
	}
	
	public static String getAttributeValue(Node node, String attributeName){
		return node.getAttributes().getNamedItem(attributeName).getTextContent();
		
	}

}
