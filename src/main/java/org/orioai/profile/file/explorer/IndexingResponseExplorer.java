/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile.file.explorer;


import java.util.ArrayList;
import java.util.List;

import org.orioai.profile.entity.RPN;
import org.orioai.profile.file.XMLParser;
import org.orioai.profile.indexing.IndexingServiceSettings;
import org.orioai.ws.indexing.IndexRecord;
import org.orioai.ws.indexing.IndexRecordFormat;
import org.orioai.ws.indexing.IndexRecordFormatLang;
import org.orioai.ws.indexing.SearchResults;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * classe qui permet de lire les reponses xml du WS de ORI-OAI-Indexing
 * @author kruczek
 *
 */
public class IndexingResponseExplorer {
	Document doc;
	Element root;
	SearchResults searchResults;
	
	 
	public IndexingResponseExplorer(SearchResults  sr) {
		searchResults=sr;
	}
	
	
	/**
	 * Retoune la liste des RPN présents dans le reponse de l'indexing
	 * @return
	 */
	public List<RPN> getRPNList(){
		List<RPN> rpns = new ArrayList<RPN>();
		if(this.searchResults==null)
			return rpns;
		
		for(int i=0;i<searchResults.getResults().size();i++){
			
			RPN rpn = getRPN(searchResults.getResults().get(i));
			if(rpn !=null)
				rpns.add( rpn );
		}
		return rpns;
	}


	/**
	 * Transforme un objet indexRecord en RPN
	 * @param rec l'objet indexRecord
	 * @return l'objet RPN
	 */
	public RPN getRPN(IndexRecord rec){
		String id = rec.getId();
		String format = rec.getUniqueFormat().getNamespace();
		String xmlContent =  getXMLContent(rec);
		
		Document doc = XMLParser.getDocumentFromString(xmlContent);
		if(doc==null)
			return null;

		Element root = doc.getDocumentElement();
			
		RPNReader rpnReader = null;	
		if(format.equals(IndexingServiceSettings.NAMESPACE_LOM))
			rpnReader=new RPNReaderLOM(root);
		if(format.equals(IndexingServiceSettings.NAMESPACE_ONIXDC))
			rpnReader= new RPNReaderONIXDC(root);
		if(rpnReader==null)
			return null;
		
		
		RPN rpn = new RPN(id);
		rpn.idUNT=rpnReader.getId();
		rpn.title=rpnReader.getTitle();
		rpn.url=rpnReader.getURL();
		rpn.date=rpnReader.getDate();
		rpn.description=rpnReader.getDescription();
		rpn.types=rpnReader.getTypes();		
		rpn.authors=rpnReader.getAuthors();
		rpn.urlPicture=rpnReader.getURLPicture();
		return rpn;
	}
	
	public String getXMLContent(IndexRecord rec){
		IndexRecordFormat irf=null;
		if(rec.getFormats().isEmpty())
			return null;
		irf= rec.getFormats().iterator().next();
		
		if(irf.getXmlContent()!=null)
			return irf.getXmlContent();
		
		IndexRecordFormatLang irfl = null;
		if(irf.getRecordFormatLangs().isEmpty())
			return null;
		irfl = irf.getRecordFormatLangs().iterator().next();
		
		return irfl.getXmlContent();
	}



}
