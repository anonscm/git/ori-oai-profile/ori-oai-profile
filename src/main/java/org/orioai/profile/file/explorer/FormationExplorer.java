/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile.file.explorer;

import java.util.ArrayList;
import java.util.List;

import org.orioai.profile.Settings;
import org.orioai.profile.entity.Formation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Classe pour recuperer des template depuis un fichier xml
 * 
 * @author kruczek
 *
 */
public class FormationExplorer {
	
	Document document;

	
	public FormationExplorer(Document doc){
		document=doc;
		
	}
	
	public List<ArrayList<String>> getIndexFormations(){
		List<ArrayList<String>> idTemps = new ArrayList<ArrayList<String>>();
		idTemps.add(new ArrayList<String>());
		idTemps.add(new ArrayList<String>());
		idTemps.add(new ArrayList<String>());
		
		if(document==null)
			return idTemps;
		
		for(int j=0;j<3;j++){
			String blocnum = String.valueOf(j+1);
			NodeList bloc = document.getElementsByTagName(Settings.XML_FORMATION_GROUP+blocnum);
			if(bloc.getLength()==0)
				return idTemps;
			
			NodeList idTags = ((Element) bloc.item(0)).getElementsByTagName(Settings.XML_FORMATION_ID);
			for (int i = 0; i<idTags.getLength(); i++) {
				String id = ((Element) idTags.item(i)).getTextContent();
				if(id!=null && id.length()!=0){
					idTemps.get(j).add(id);
				}
			}
		}
		return idTemps;
	}
	
	/**
	 * Retourne les templates du document xml en fonction 
	 * @return liste des templates
	 */
	public List<Formation> getFormations(){
		List<Formation> templates = new ArrayList<Formation>();
		if(document==null)
			return templates;
		
		NodeList nodes = document.getElementsByTagName(Settings.TAG_FORMATION);
	
		for (int i = 0; i<nodes.getLength(); i++) {
			Element elem = (Element) nodes.item(i);
			
			Formation temp = createFormation(elem);
			if(temp!=null){
				templates.add(temp);
			}
		}
		return templates;
	}
	
	/**
	 * Creation d'un template a partir d'un element xml
	 * @param elem l'element xml
	 * @return un template, null sinon
	 */
	public Formation createFormation(Element elem){		
		Formation temp = new Formation();
		
		String id=  getTagValue(elem, Settings.XML_FORMATION_ID);
		String name = getTagValue(elem, Settings.XML_FORMATION_NAME);
		List<String> deweys =  this.getTagValues(elem,Settings.XML_FORMATION_DEWEY);
		List<String> keys = this.getTagValues(elem, Settings.XML_FORMATION_KEYWORD); 
		List<String> green = this.getGreenList(elem);
		List<String> red = this.getRedList(elem);
		
 		if(id==null || name==null || id.isEmpty() || name.isEmpty()){
			return null;
		}
		
		temp.setId(id);
		temp.setName(name);
		temp.setDeweys(deweys);
		temp.setKeywords(keys);
		temp.setGreen(green);
		temp.setRed(red);
		return temp;
	}
	
	/**
	 * Recuperation de la liste verte dans le xml du template
	 * @param elem le xml sous forme Element
	 * @return la liste verte
	 */
	public List<String> getGreenList(Element elem){
		Node node=elem.getElementsByTagName(Settings.XML_FORMATION_GREEN).item(0);
		if(node==null)
			return new ArrayList<String>();
		return getTagValues((Element)node, Settings.XML_FORMATION_RPN);
	}
	
	/**
	 * Recuperation de la liste rouge dans le xml du template
	 * @param elem le xml sous forme Element 
	 * @return la liste rouge
	 */
	public List<String> getRedList(Element elem){
		Node node=elem.getElementsByTagName(Settings.XML_FORMATION_RED).item(0);
		if(node==null)
			return new ArrayList<String>();
		return getTagValues((Element)node, Settings.XML_FORMATION_RPN);
	}
	
	
	
	/**
	 * Retourne la value d'une balise d'un element xml
	 * @param elem element xml
	 * @param tagName nom balise
	 * @return la valeur 
	 */
	public String getTagValue(Element elem, String tagName){
		Node node = elem.getElementsByTagName(tagName).item(0);
		if(node!=null){
			return node.getTextContent();
		}
		return null;
	}
	
	
	/**
	 * Retourne la liste des valeurs d'une balise dans une element xml
	 * @param elem element xml
	 * @param tagName nom de la balise
	 * @return la liste des valeurs
	 */
	public List<String> getTagValues(Element elem, String tagName){
		List<String> list = new ArrayList<String>();
		NodeList nodes= elem.getElementsByTagName(tagName);
		
		for(int j=0;j<nodes.getLength();j++){
			list.add(nodes.item(j).getTextContent());
		}
		
		return list;
	}

	
}
