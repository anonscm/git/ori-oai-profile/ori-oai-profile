package org.orioai.profile.file.explorer;

import java.util.Arrays;
import java.util.List;

import org.orioai.profile.file.XMLParser;
import org.orioai.profile.indexing.IndexingServiceSettings;
import org.w3c.dom.Element;

/**
 * Classe pour recuperer des informations dans du XML ONIX_DC
 * @author kruczek
 *
 */
public class RPNReaderONIXDC implements RPNReader {
	
	Element root;
	
	public RPNReaderONIXDC(Element root) {
		this.root=root;
	}

	@Override
	public String getId() {
		return XMLParser.getString(IndexingServiceSettings.PATH_ONIXDC_IDENTIFIER, root);
	}

	@Override
	public String getTitle() {
		String title=XMLParser.getString(IndexingServiceSettings.PATH_ONIXDC_TITLE, root);
		//System.out.println("title="+title);
		return title;
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public String getURL() {
		return XMLParser.getString(IndexingServiceSettings.PATH_ONIXDC_URL, root);
	}

	@Override
	public String getDate() {
		return XMLParser.getString(IndexingServiceSettings.PATH_ONIXDC_DATE, root);
	}

	@Override
	public List<String> getAuthors() {
		List<String> authors= XMLParser.getStringList(
				IndexingServiceSettings.PATH_ONIXDC_AUTHORS, root);		
		return authors;
	}

	@Override
	public List<String> getTypes() {
		return Arrays.asList(IndexingServiceSettings.ONIXDC_TYPE);
	}

	@Override
	public String getURLPicture() {
		return XMLParser.getString(IndexingServiceSettings.PATH_ONIXDC_PICTURE, root);
	}

}
