package org.orioai.profile.file.explorer;

import java.util.List;


/**
 * interface pour lire une RPN a partir du XML 
 * 
 * @author kruczek
 *
 */
public interface RPNReader {
	
	public String getId();
	
	public String getTitle();

	public String getDescription();
	
	public String getURL();
	
	public String getDate();
 
	public List<String> getAuthors();
	
	public List<String> getTypes();
	
	public String getURLPicture();


}
