package org.orioai.profile.ldap;


import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.apache.log4j.Logger;

public class LDAPServer {
	private static Logger log = Logger.getLogger(LDAPServer.class);
	public LDAPConfiguration config=null;
	
	public LDAPServer() {
	}
	
	public LDAPServer(LDAPConfiguration ldapConf) {
		config=ldapConf;
	}
	
	/**
	 * Effectue la connexion au serveur ldap
	 * @return la connexion au ldap
	 */
	public DirContext makeConnection(){
		DirContext context=null;
		
		try {
			context = new InitialDirContext(config.getEnvironnment());
			log.debug("[LDAP] Connection to ldap server : SUCCES");

		} catch (NamingException e) {
			log.error("[LDAP] Connection to ldap server : FAIL");
			e.printStackTrace();
		}
		
		return context;
	}
	
	/**
	 * Make request to ldap server to get all attributes for uid
	 * @param uid the uid 
	 * @return the attributes
	 * @throws Exception
	 */
	public Attributes getAttributes(String uid, String[] attrIds){
		DirContext context =makeConnection();
		Attributes attrs=null;
		
		if(context==null)
			return null;
		
		try {
			attrs = context.getAttributes(config.getTreePath(uid),null);		
		} catch (NamingException e) {
			log.error("[LDAP] impossible to get attributes for '"+uid+
					"' "+attrIds);
			log.error(e);
		}

		try {
			context.close();
		} catch (NamingException e) {
			log.error("[LDAP] impossible to close ldap server connection");
			log.error(e);
		}
		return attrs;
	}

}
