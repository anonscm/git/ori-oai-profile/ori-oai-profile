/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile.ldap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import org.apache.log4j.Logger;
import org.orioai.profile.Settings;
import org.orioai.profile.file.PropertiesFileParser;



/**
 * Classe principale pour la gestion des informations du ldap
 * @author kruczek
 *
 */
public class LDAPService {
	private static Logger log = Logger.getLogger(LDAPService.class);
	private static LDAPService instance; 
	LDAPServer server = null;

	
	private LDAPService(){
		log.debug("[LDAP] initialization");
		init();
	}
	
	/**
	 * Recupere l'instance de LDAPService (singleton)
	 * @return
	 */
	public static synchronized LDAPService getInstance() {
		if (instance == null) {
			instance = new LDAPService();
		}
		return instance;
	}
	
	/**
	 * Initialisation du service
	 */
	public void init(){
		PropertiesFileParser pfp = new PropertiesFileParser();
		Properties prop = pfp.getProperties(Settings.PATH_CONFIG);
		server=new LDAPServer(new LDAPConfiguration(prop));
	}
	
	public void setServer(LDAPServer s){
		server=s;
	}
	
	/**
	 * Recuperation des informations sur le ldap
	 * @param uid l'uid de l'etudiant
	 * @return les informations
	 */
	public Map<String,List<String>> getLDAPValues(String uid){
		Map<String,List<String>> map = new HashMap<String, List<String>>();
		if(uid==null)
			return null;
		
		if(server==null)
			return map;
		
		String[] idAttrs = new String[]{
				Settings.LDAP_ID_FORMATION,
				Settings.LDAP_DISPLAYNAME};
		Attributes attrs = server.getAttributes(uid,idAttrs);
		if(attrs==null)
			return map;
		
		//System.out.println("[LDAP] recuperation de "+attrs.size()+" attributs pour '"+uid+"'");
		//showAttributes(attrs);	
		
		Attribute attrForm = attrs.get(Settings.LDAP_ID_FORMATION);
		if(attrForm==null)
			log.debug("[LDAP] Attribute '"+Settings.LDAP_ID_FORMATION
					+"' not found for '"+uid+"'");
		map.put(Settings.LDAP_ID_FORMATION, getIdFormations(attrForm));

		
		Attribute attrName = attrs.get(Settings.LDAP_DISPLAYNAME);
		if(attrName==null)
			log.debug("[LDAP] Attribute '"+Settings.LDAP_DISPLAYNAME
					+"' not found for '"+uid+"'");
		map.put( Settings.LDAP_DISPLAYNAME,Arrays.asList(getValue(attrName,0)) );
		
		return map;
	}
	
	
	/**
	 * Retourne les id des formations d'un etudiant
	 * @param uid UID de l'etudiant
	 * @return la liste des id de formations
	 */
	public List<String> getIdFormations(Attribute attr){
		List<String> formations = new ArrayList<String>();
		
		if(attr==null)
			return formations;
		

		for(int i=0;i<attr.size();i++){
			String idFormation = cleanValue(getValue(attr,i));
			if(idFormation!=null){
				idFormation= idFormation.split("-")[0];
				formations.add(idFormation);
			}
		}

		return formations;
	}
		
	
	/**
	 * Recupère une valeur d'un attribut
	 * @param attr l'attribut contenant la valeur
	 * @param i l'indice de la valeur
	 * @return la valeur, null sinon
	 */
	public String getValue(Attribute attr, int i){
		String value= null;
		if(attr==null || attr.size()-1<i)
			return value;
		
		try {
			value= attr.get(i).toString();
		} catch (NamingException e) {
			log.error("[LDAP] Error when extract attribute value of "+attr.getID());
			log.error(e);
		}
		return value;
	}
	
	/**
	 * Nettoie la valeur d'un attribut du ldap
	 * @param attr attribut du ldap
	 * @param index index de la valeur
	 * @return la valeur nettoyé
	 */
	public String  cleanValue(String value){
		if(value==null)
			return null;
		String clean=value;
		if (value.contains("}")){
			clean = value.split("}")[1];
		}

		return clean;
	}
	
	
	/**
	 * Affiche les valeurs de chaque attributs
	 * @param attrs les attributs
	 */
	public void showAttributes(Attributes attrs){
		log.debug("[LDAP] number of attributes : "+attrs.size());
		
		try {
	        for (NamingEnumeration ae = attrs.getAll(); ae.hasMore();) {
	          Attribute attr = (Attribute) ae.next();

	          /* print each value */
	          for (NamingEnumeration e = attr.getAll(); 
	        		  e.hasMore(); 
	        		  log.debug(attr.getID()+" : " + e.next()));
	        }
	     } catch (NamingException e) {
	        e.printStackTrace();
	     }
	}
	
	
	

}
