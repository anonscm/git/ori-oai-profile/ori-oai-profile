/*
    Copyright (C) 2017 Kruczek Rémi

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. 
*/
package org.orioai.profile.ldap;

import java.util.Hashtable;
import java.util.Properties;

import javax.naming.Context;


/**
 * Classe qui contient les valeurs pour configurer l'acces au ldap
 * @author kruczek
 *
 */
public class LDAPConfiguration {
	private String protocole;
	private String base;
	private String people;
	private String id;
	private String idFormation;
	private String serverAdr = null;
	private String login =null;
	private String password = null;
	
	
	/**
	 * Constructeur qui initialise les valeurs de connexion
	 * @param fm le gestionnaire des fichiers 
	 */
	public LDAPConfiguration(Properties props){
		if(props==null){
			return;
		}
		protocole =	props.getProperty("LDAP_PROTOCOLE");
		serverAdr =	props.getProperty("LDAP_SERVER");
		base=	props.getProperty("LDAP_BASE");
		people= props.getProperty("LDAP_PEOPLE");
		id= 	props.getProperty("LDAP_UID");
		idFormation = props.getProperty("LDAP_FORMATION");
		login= 	props.getProperty("LDAP_LOGIN");
		password= 	props.getProperty("LDAP_PASS");
	}
	
	public String getProtocole(){
		return protocole;
	}
	public String getBase(){
		return base;
	}
	public String getPeople(){
		return people;
	}
	public String getId(){
		return id;
	}
	public String getIdFormation(){
		return idFormation;
	}
	public String getServAdr(){
		return serverAdr;
	}
	public String getLogin(){
		return login;
	}
	public String getPass(){
		return password;
	}
	
	public void setIdFoormation(String id){
		idFormation=id;
	}
	
	/**
	 * Retourne l'environnement permettant la connexion au ldap.
	 * @return l'environnement
	 */
	public Hashtable<String,String> getEnvironnment(){
		Hashtable<String,String> environnement = new Hashtable<String,String>();
		environnement.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		environnement.put(Context.PROVIDER_URL, protocole+"://"+serverAdr+"/");
		environnement.put(Context.SECURITY_AUTHENTICATION, "simple");
		environnement.put(Context.SECURITY_PRINCIPAL, login);
		environnement.put(Context.SECURITY_CREDENTIALS, password);
		return environnement;
	}
	
	/**
	 * Retourne le chemin qui identifie un etudiant sur le ldap
	 * @param uid uid de l'etudiant
	 * @return le chemin ldap
	 */
	public String getTreePath(String uid){
		String path=id+"="+uid+","+people+","+base;;
		if(people.isEmpty()){
			path=id+"="+uid+","+base;
		}
		return path;
	}
	

}
