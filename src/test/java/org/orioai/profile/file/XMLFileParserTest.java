package org.orioai.profile.file;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.orioai.profile.entity.MockData;
import org.orioai.profile.file.XMLParser;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLFileParserTest {
	
	@BeforeClass
	public static void init(){
		Logger.getRootLogger().setLevel(Level.OFF);
		Logger.getLogger("org.orioai").setLevel(Level.OFF);

	}
	
	@Test
	public void testGetDocumentFromFile(){
		XMLParser xp = new XMLParser();
		Document doc = xp.getDocumentFromFile("/file.xml");
		assertTrue(doc!=null);
		
		doc = xp.getDocumentFromFile("fail");
		assertTrue(doc==null);
	}
	
	
	@Test
	public void testGetDocumentFromString() throws SAXException, IOException, ParserConfigurationException{
		String xml="<test>toto</test>";
		Document doc =XMLParser.getDocumentFromString(xml);
		assertTrue(doc.getElementsByTagName("test").item(0).getTextContent().equals("toto"));
		
		xml="<test>toto</teste>";
		doc =XMLParser.getDocumentFromString(xml);
		assertTrue(doc==null);
	
	}
	
		
	@Test
	public void testGetString() throws SAXException, IOException, ParserConfigurationException{
		String xml = "<toto><titi>test</titi></toto>";
		Document doc= MockData.getDocument(xml);

		String s = XMLParser.getString("//titi",doc.getDocumentElement());
		assertTrue(s.equals("test"));
	
	}
	
	@Test
	public void testGetNode() throws SAXException, IOException, ParserConfigurationException{
		String xml = "<toto><titi>test1</titi><titi>test2</titi></toto>";
		Document doc= MockData.getDocument(xml);
		
		NodeList list= XMLParser.getNodeList("//titi",doc.getDocumentElement());
		
		assertTrue(list.getLength()==2);
		assertTrue(list.item(1).getTextContent().equals("test2"));
	}


}
