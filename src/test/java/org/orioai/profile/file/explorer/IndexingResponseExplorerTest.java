package org.orioai.profile.file.explorer;

import static org.junit.Assert.*;

import org.junit.Test;
import org.orioai.profile.entity.MockData;
import org.orioai.profile.entity.RPN;
import org.orioai.profile.file.explorer.IndexingResponseExplorer;
public class IndexingResponseExplorerTest {
	
	IndexingResponseExplorer ire= new IndexingResponseExplorer(MockData.getSearchResults());
	
	
	@Test
	public void testGetRPNList(){
		assertTrue(ire.getRPNList().size()==2);
		assertTrue(ire.getRPNList().get(0).id.equals(MockData.getGoodID()));
	}
	
	@Test
	public void testGetRPN(){
		RPN rpn = ire.getRPN(MockData.getRecord1());
		assertTrue(rpn.id.equals(MockData.getGoodID()));
		
	}
	


	



}
