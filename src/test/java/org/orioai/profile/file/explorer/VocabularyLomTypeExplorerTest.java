package org.orioai.profile.file.explorer;

import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;
import org.orioai.profile.entity.MockData;
import org.orioai.profile.file.XMLParser;
import org.orioai.profile.file.explorer.VocabularyLomTypeExplorer;
import org.w3c.dom.Document;

public class VocabularyLomTypeExplorerTest {

	
	@Test
	public void testGetMap(){
		
		String types = MockData.getTypeLomExemple();
		
		Document doc = XMLParser.getDocumentFromString(types);
		
		Map<String,String> map = VocabularyLomTypeExplorer.getType(doc);
		
		
		assertTrue(map.containsKey("exercise"));
		assertTrue(map.containsValue("graphe"));
		//System.out.println(map);
	}
	
}
