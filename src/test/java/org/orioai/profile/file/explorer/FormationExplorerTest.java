package org.orioai.profile.file.explorer;

import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.orioai.profile.entity.Formation;
import org.orioai.profile.entity.MockData;
import org.orioai.profile.file.explorer.FormationExplorer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class FormationExplorerTest {
	
	String xmltemp1="<formation><id></id><name></name>"
			+ "<deweys/>"
			+ "<keywords/>"
			+ "</formation>";
	
	String xmltemp2="<formation>"
			+ "<id>IDT</id>"
			+ "<name>Nom du T</name>"
			+ "<deweys/>"
			+ "<keywords/>"
			+ "<green><rpn>rpngreen1</rpn><rpn>rpngreen2</rpn></green>"
			+ "<red><rpn>rpngreen3</rpn></red>"
			+ "</formation>";
	
	String xmltemp3="<formation>"
			+ "<id>temp3</id>"
			+ "<name>nom template 3</name>"
			+ "</formation>";
	
	
	String xml = "<formations>"
			+ xmltemp1
			+ xmltemp2
			+xmltemp3
			+ "</formations>";
	
	public FormationExplorer te= new FormationExplorer(MockData.getDocument(xml));
	

	@BeforeClass
	public static void init(){
		
		Logger.getLogger("org.orioai").setLevel(Level.OFF);

	}
	
	
	

	
	
	@Test
	public void testGetFormations(){
		List<Formation> temps = te.getFormations();
		assertTrue(temps.size()==2);
		assertTrue(temps.get(0).getId().equals("IDT"));
		assertTrue(temps.get(0).getGreen().size()==2);
		assertTrue(temps.get(0).getRed().size()==1);
		assertTrue(temps.get(1).getRed().size()==0);
		assertTrue(temps.get(1).getDeweys().size()==0);
		
	}
	
	@Test
	public void testCreateFormation() throws ParserConfigurationException {
		Element elem = getFormationElem("TestID", "TestName");
        Formation temp = te.createFormation(elem);
        assertTrue(temp.getId().equals("TestID"));
       
        
        elem = getFormationElem("","test");
        temp = te.createFormation(elem);
        assertTrue(temp==null);
        
        elem = getFormationElem(null, "test");
        temp = te.createFormation(elem);
        assertTrue(temp==null);
		
	}
	
	public Element getFormationElem(String id, String name) throws ParserConfigurationException{
		Document doc = MockData.getDocument(); 
		
	    Element root = doc.createElement("template");
        Node node = doc.createElement("id");
        node.setTextContent(id);
        root.appendChild(node);
        node = doc.createElement("name");
        node.setTextContent(name);
        root.appendChild(node);
        return root;
	}
	
	public Element getListElem(int size, String tagName) throws ParserConfigurationException{
		
		Document doc = MockData.getDocument();
	    Element root = doc.createElement("root");
	    Node node;
	    for(int i=0;i<size;i++){
	    	node = doc.createElement(tagName);
	        root.appendChild(node);
	    }
        return root;
	}
	
	
	@Test
	public void testGetTagValue() throws ParserConfigurationException{
		Element elem = getFormationElem("TestFormID", "TestFormName");
		String str = te.getTagValue(elem, "id");
		assertTrue(str.equals("TestFormID"));
		str = te.getTagValue(elem, "toto");
		assertTrue(str==null);
		
		
	}
	@Test
	public void testGetTagValues() throws ParserConfigurationException{
		Element elem = getListElem(5,"toto");
		assertTrue(te.getTagValues(elem, "toto").size()==5);
		assertTrue(te.getTagValues(elem, "tott").size()==0);
		
	}

}
