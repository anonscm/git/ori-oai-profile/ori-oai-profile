package org.orioai.profile.file;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.orioai.profile.file.PropertiesFileParser;

public class PropertiesFileParserTest {
	
	@BeforeClass
	public static void init(){
		Logger.getRootLogger().setLevel(Level.OFF);
		Logger.getLogger("org.orioai").setLevel(Level.OFF);

	}
	
	@Test
	public void testGetProperties(){
		PropertiesFileParser pfp = new PropertiesFileParser();
		
		Properties props = pfp.getProperties("fail");
		assertTrue(props.size()==0);
		
		props = pfp.getProperties("/file.properties");
		assertTrue(props.containsKey("LABEL"));
		assertTrue(props.containsValue("value"));
		assertTrue(!props.containsKey("LABEL "));
		assertTrue(!props.containsValue("value "));
		
	}
}
