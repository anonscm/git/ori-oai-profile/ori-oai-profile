package org.orioai.profile.indexing;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.orioai.profile.entity.IndexingResponse;
import org.orioai.profile.indexing.IndexingClient;
import org.orioai.ws.indexing.OriOaiIndexingServiceInterface;

public class IndexingClientTest {
	static OriOaiIndexingServiceInterface inter;
	static IndexingClient ic ;
	
	
	@BeforeClass
	public static void init(){
		
		Logger.getLogger("org.orioai").setLevel(Level.OFF);
		 inter= new MockIndexingInterface();
		 ic = new IndexingClient(inter);
	}
		
	@Test
	public void testSXD(){
		IndexingResponse response =ic.searchXMLDocs("", 1, 1);	
		assertTrue(response.rpns.size()==2);
		response =ic.searchXMLDocs("fail", 1, 1);	
		assertTrue(response.rpns.size()==0);
		
	}
	
	@Test
	public void testSFA(){
		IndexingResponse response =ic.searchFromAttributes("", 1, 1);	
		assertTrue(response.rpns.size()==2);
		response =ic.searchXMLDocs("fail", 1, 1);	
		assertTrue(response.rpns.size()==0);
	}

}
