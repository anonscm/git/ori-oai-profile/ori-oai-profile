package org.orioai.profile.indexing;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.orioai.profile.entity.Formation;
import org.orioai.profile.entity.IndexingResponse;
import org.orioai.profile.entity.MockData;
import org.orioai.profile.entity.RPN;
import org.orioai.profile.indexing.IndexingService;

public class IndexingServiceTest {
	static IndexingClient ic;
	static IndexingService is;
	
	@BeforeClass
	public static void init(){
		Logger.getLogger("org.orioai").setLevel(Level.OFF);
		ic =new IndexingClient(new MockIndexingInterface());
	    is = new IndexingService(ic);
	}
	
	
	@Test
	public void testGetRPN(){
		RPN rpn = is.getRPNWithId(MockData.getGoodID());
		assertTrue(rpn.id.equals(MockData.getGoodID()));
	}
	
	@Test
	public void testGetRPNWithString(){
		
		IndexingResponse ir = is.getRPNsWithWords(Arrays.asList(MockData.getGoodTitle()), 1, 1);
		assertTrue(ir.rpns.size()==2);
		assertTrue(ir.totalDoc==2);
	}
	
	@Test
	public void testGetRPNsWithTemps(){
		Formation temp = new Formation( "", "",
				Arrays.asList("toto"),
				Arrays.asList(MockData.getGoodTitle()) );
		IndexingResponse ir = is.getRPNsWithFormations(Arrays.asList(temp), 1, 1);
		//System.out.println(ir.totalDoc+"");
		assertTrue(ir.rpns.size()==2);
		assertTrue(ir.rpns.get(0).authors.contains("pre1 nom1"));
		assertTrue(ir.totalDoc==2);
		
	}
	
	

}
