package org.orioai.profile.indexing;

import java.util.List;
import java.util.Map;

import org.orioai.profile.entity.MockData;
import org.orioai.ws.indexing.IndexRecord;
import org.orioai.ws.indexing.OriOaiIndexingServiceInterface;
import org.orioai.ws.indexing.SearchResults;
import org.orioai.ws.indexing.SearchTagElements;
import org.orioai.ws.indexing.exceptions.ISDeleteException;
import org.orioai.ws.indexing.exceptions.ISIndexException;
import org.orioai.ws.indexing.exceptions.ISSearchException;


public class MockIndexingInterface implements OriOaiIndexingServiceInterface {

	@Override
	public int deleteRecord(String arg0) throws ISDeleteException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteRecord(String arg0, String arg1) throws ISDeleteException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int[] deleteRecords(String[] arg0) throws ISDeleteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] deleteRecords(String arg0, String[] arg1) throws ISDeleteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int[] deleteRecords(String[] arg0, String[] arg1) throws ISDeleteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getDeadlinkRecordsIds() throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getDeadlinkRecordsIds(String arg0) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getDeadlinkRecordsIds(String arg0, String arg1) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDefinitiveDeadLinks() throws ISIndexException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchTagElements getMergedTags(List<String> arg0, int arg1) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchTagElements getMergedTags(List<String> arg0, int arg1, String arg2) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchTagElements getMergedTags(String arg0, String arg1, List<String> arg2, int arg3)
			throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchTagElements getMergedTags(String arg0, String arg1, List<String> arg2, int arg3, String arg4)
			throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getRecordSetList() throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchTagElements getTags(String arg0, String arg1, List<String> arg2) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchTagElements getTags(String arg0, String arg1, List<String> arg2, int arg3) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchTagElements getTags(String arg0, String arg1, List<String> arg2, String arg3)
			throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchTagElements getTags(String arg0, String arg1, List<String> arg2, String arg3, int arg4)
			throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchTagElements getTags(String arg0, String arg1, List<String> arg2, int arg3, String arg4)
			throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchTagElements getTags(String arg0, String arg1, List<String> arg2, String arg3, int arg4, String arg5)
			throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int index(String arg0, String arg1, String arg2, String arg3, String arg4, boolean arg5)
			throws ISIndexException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int indexOrUpdate(String arg0, String arg1, String arg2, String arg3, String arg4) throws ISIndexException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int indexOrUpdate(String arg0, String arg1, String arg2, String arg3, String arg4, boolean arg5)
			throws ISIndexException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int indexOrUpdate(String arg0, String arg1, String arg2, String arg3, String arg4, String arg5)
			throws ISIndexException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int indexOrUpdate(String arg0, String arg1, String arg2, String arg3, String arg4, boolean arg5, String arg6)
			throws ISIndexException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int indexOrUpdate(String arg0, String arg1, String arg2, String arg3, String arg4, String arg5, boolean arg6)
			throws ISIndexException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public long searchForNumberOfResults(String arg0, String arg1) throws ISSearchException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Long> searchForNumberOfResults(List<String> arg0, String arg1) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long searchForNumberOfResults(String arg0, String arg1, String arg2) throws ISSearchException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Long> searchForNumberOfResults(List<String> arg0, String arg1, String arg2) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchResults searchFromAttributes(String arg0, String arg1, int arg2, int arg3, List<String> arg4)
			throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchResults searchFromAttributes(String arg0, String arg1, Map<String, List<String>> arg2, int arg3,
			int arg4, List<String> arg5) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchResults searchFromAttributes(String arg0, String arg1, List<String> arg2, int arg3, int arg4,
			List<String> arg5, List<Boolean> arg6, List<String> arg7) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchResults searchFromAttributes(String arg0, String arg1, List<String> arg2, int arg3, int arg4,
			List<String> arg5, List<Boolean> arg6, List<String> arg7, String arg8) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchResults searchFromAttributes(String arg0, String arg1, Map<String, List<String>> arg2,
			List<String> arg3, int arg4, int arg5, List<String> arg6, List<Boolean> arg7, List<String> arg8)
			throws ISSearchException {
		// TODO Auto-generated method stub
		if(arg0.contains("fail"))
			return new SearchResults();
		return MockData.getSearchResults();
	}

	@Override
	public SearchResults searchFromAttributes(String arg0, String arg1, Map<String, List<String>> arg2,
			List<String> arg3, int arg4, int arg5, List<String> arg6, List<Boolean> arg7, List<String> arg8,
			String arg9) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchResults searchIds(String arg0, String arg1, int arg2, int arg3) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchResults searchIds(String arg0, String arg1, int arg2, int arg3, String arg4) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchResults searchMoreLikeThis(String arg0, String arg1, Map<String, List<String>> arg2, List<String> arg3,
			int arg4, List<String> arg5) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchResults searchMoreLikeThis(String arg0, String arg1, Map<String, List<String>> arg2, List<String> arg3,
			int arg4, List<String> arg5, String arg6) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IndexRecord searchXMLDoc(String arg0) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IndexRecord searchXMLDoc(String arg0, String arg1) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IndexRecord searchXMLDoc(String arg0, String arg1, String arg2) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchResults searchXMLDocs(String arg0, String arg1, int arg2, int arg3) throws ISSearchException {
		// TODO Auto-generated method stub
		if(arg0.contains("fail"))
			return new SearchResults();
		return MockData.getSearchResults();
		
	}

	@Override
	public SearchResults searchXMLDocs(String arg0, String arg1, int arg2, int arg3, String arg4)
			throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchResults searchXMLDocs(String arg0, String arg1, String arg2, int arg3, int arg4)
			throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchResults searchXMLDocs(String arg0, String arg1, List<String> arg2, int arg3, int arg4)
			throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchResults searchXMLDocs(String arg0, String arg1, String arg2, int arg3, int arg4, String arg5)
			throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SearchResults searchXMLDocs(String arg0, String arg1, List<String> arg2, int arg3, int arg4, String arg5)
			throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] uniqueValues(String arg0) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] uniqueValues(String arg0, String arg1) throws ISSearchException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int update(String arg0, String arg1, String arg2, String arg3, String arg4, boolean arg5)
			throws ISIndexException, ISDeleteException {
		// TODO Auto-generated method stub
		return 0;
	}

}
