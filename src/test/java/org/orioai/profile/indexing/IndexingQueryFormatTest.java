package org.orioai.profile.indexing;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.orioai.profile.indexing.IndexingQueryFormat;

public class IndexingQueryFormatTest {
	List<String> deweys =Arrays.asList("502", "300 99", "504.51");
	List<String> keys =Arrays.asList("gestion projet", "économie");
	List<String> greens =Arrays.asList("id1", "id5");
	List<String> reds =Arrays.asList("id2", "id6");
	
	@Test
	public void getQueryTest(){
		String q = IndexingQueryFormat.getQuery(deweys,keys,greens,reds);
		//System.out.println(q);
		assertTrue(q.contains("gestion_projet"));
		assertTrue(q.contains("economie"));
		assertTrue(q.contains("300?99"));
		assertTrue(q.contains("504.51"));
		assertTrue(q.contains(" OR "));
		assertTrue(q.contains("md-ori-oai-id"));
		
		assertTrue(q.contains("lom.general.title_fr"));
		assertTrue(q.contains("lom.general.description_fr"));
		
		assertTrue(q.contains("onixdc.title"));
		assertTrue(q.contains("onixdc.subject_fr"));
		assertTrue(q.contains("onixdc.Product.Subject.SubjectCode"));	
	}
	
	@Test
	public void testGetQueryForWords(){
		String q = IndexingQueryFormat.getQuery(null, Arrays.asList("fusion","algèbre linéaire"), null, null);
		//System.out.println(q);
		assertTrue(q.contains("fusion"));
		assertTrue(q.contains("algebre_lineaire"));
		assertTrue(q.contains("lom.general.title_fr"));
		assertTrue(q.contains("onixdc.title"));		
	}
	
	@Test
	public void testGetQueryDeweys(){
		String q= IndexingQueryFormat.getQuery(deweys, null, null, null);
		assertTrue(q.contains("lom.classification.ddc.id"));
	}

	@Test
	public void testGetQueryRed(){
		String q= IndexingQueryFormat.getQuery(null, null, null, reds);
		assertTrue(q.contains("NOT md-ori-oai-id"));
	}
	@Test
	public void testGetQueryForID(){
		String q= IndexingQueryFormat.getQuery(null, null, Arrays.asList("toto"), null);
		assertTrue(q.contains("md-ori-oai-id:(\"toto\")"));
	}
	@Test
	public void testGetQueryGreenRed(){
		String q= IndexingQueryFormat.getQuery(null, null, greens, reds);
		assertTrue(q.contains("(md-ori-oai-id"));
		assertTrue(q.contains("NOT md-ori-oai-id"));
	}
	
	
}
