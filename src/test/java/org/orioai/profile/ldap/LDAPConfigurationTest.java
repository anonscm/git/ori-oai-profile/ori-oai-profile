package org.orioai.profile.ldap;

import static org.junit.Assert.assertTrue;

import java.util.Properties;

import org.junit.Test;
import org.orioai.profile.entity.MockData;
import org.orioai.profile.ldap.LDAPConfiguration;

public class LDAPConfigurationTest {
	
	@Test
	public void testLDAPConfiguration(){
		
		LDAPConfiguration config =  new LDAPConfiguration(MockData.getLDAPProperties());
		assertTrue(config.getServAdr().equals("ldap.forumsys.com:389"));
		
		config =  new LDAPConfiguration(new Properties());
		assertTrue(config.getServAdr()==null);
	}

}
