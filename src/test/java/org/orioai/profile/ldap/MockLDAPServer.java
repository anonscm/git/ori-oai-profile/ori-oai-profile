package org.orioai.profile.ldap;

import javax.naming.directory.Attributes;

import org.orioai.profile.entity.MockData;
import org.orioai.profile.ldap.LDAPServer;


public class MockLDAPServer extends LDAPServer{

	
	public MockLDAPServer() {
		
	}

	/**
	 * Fake response from ldap server
	 */
	public Attributes getAttributes(String uid, String[] attrIds){
		if(uid.equals(MockData.getGoodUID()))
			return MockData.getLDAPAttributes();
		else 
			return null;
	}

}
