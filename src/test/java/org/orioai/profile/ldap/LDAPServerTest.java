package org.orioai.profile.ldap;

import static org.junit.Assert.assertTrue;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import org.junit.Test;
import org.orioai.profile.entity.MockData;
import org.orioai.profile.ldap.LDAPServer;

public class LDAPServerTest {

	/**
	 * le serveur peut etre inaccessible 
	 *  donc le test fail
	 * @throws NamingException
	 */
	@Test
	public void testMakeConnection() throws NamingException{
		
		/*LDAPServer serv = new LDAPServer(MockData.getLDAPConfig());
		
		DirContext dc = serv.makeConnection();
		
		assertTrue(dc!=null);
		dc.close();*/
	}
	
	

	@Test
	public void testGetAttributes(){
		LDAPServer serv = new MockLDAPServer();
		
		Attributes attr = serv.getAttributes(MockData.getGoodUID(), null);
		assertTrue(attr!=null);
	}
}
