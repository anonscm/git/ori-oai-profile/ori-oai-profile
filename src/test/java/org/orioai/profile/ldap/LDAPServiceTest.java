package org.orioai.profile.ldap;

import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;

import javax.naming.directory.Attribute;
import javax.naming.directory.BasicAttribute;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.orioai.profile.Settings;
import org.orioai.profile.entity.MockData;
import org.orioai.profile.ldap.LDAPService;

public class LDAPServiceTest {

	static LDAPService ldaps;
	

	@BeforeClass
	public static void init(){
		Logger.getLogger("org.orioai").setLevel(Level.OFF);
		ldaps = LDAPService.getInstance();
	}
	
	@Test
	public void testGetLDAPValues(){
		Settings.LDAP_ID_FORMATION=MockData.getLDAPFormationAttr();
		Settings.LDAP_DISPLAYNAME=MockData.getLDAPDisplayNameAttr();
		
		ldaps.server=new MockLDAPServer();	
		Map<String,List<String>> map = ldaps.getLDAPValues(MockData.getGoodUID());
		
		assertTrue(map.get(Settings.LDAP_ID_FORMATION).size()==1);
		assertTrue(map.get(Settings.LDAP_DISPLAYNAME).size()==1);
		assertTrue(map.get(Settings.LDAP_DISPLAYNAME).get(0).equals("Toto Titi"));
		assertTrue(map.get("testko")==null);
	}

	
	@Test
	public void testGetFormations(){
		Settings.LDAP_ID_FORMATION=MockData.getLDAPFormationAttr();
		Attribute attr= new BasicAttribute(Settings.LDAP_ID_FORMATION, "IDFORM1");
		
		List<String> forms = ldaps.getIdFormations(attr);
		
		assertTrue(forms.size()==1);
		assertTrue(forms.get(0).equals("IDFORM1"));
	}
	
	@Test
	public void testGetValue(){
		Attribute attr = new BasicAttribute("attrId","valeur");

		assertTrue(ldaps.getValue(attr, 0).equals("valeur"));
	}
	
	@Test
	public void testCleanValue(){

		assertTrue(ldaps.cleanValue("{1}test").equals("test"));
		assertTrue(ldaps.cleanValue("test").equals("test"));
	}
}
