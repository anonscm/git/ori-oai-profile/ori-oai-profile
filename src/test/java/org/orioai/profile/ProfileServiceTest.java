package org.orioai.profile;

import static org.junit.Assert.assertTrue;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.orioai.profile.ProfileService;
import org.orioai.profile.Settings;
import org.orioai.profile.entity.MockData;
import org.orioai.profile.entity.Profile;
import org.orioai.profile.ldap.MockLDAPServer;

public class ProfileServiceTest {
	
	@BeforeClass
	public static void init(){
		Logger.getLogger("org.orioai").setLevel(Level.OFF);

	}
	
	
	
	@Test
	public void testCreateProfile(){
		ProfileService ps = ProfileService.getInstance();
		ps.init();
		ps.ldapS.setServer(new MockLDAPServer());
		ps.ts.setFormations(MockData.getFormations());
		Settings.LDAP_ID_FORMATION=MockData.getLDAPFormationAttr();
		Settings.LDAP_DISPLAYNAME=MockData.getLDAPDisplayNameAttr();
		
		
		String uid = MockData.getGoodUID();
		Profile p = ps.getProfile(uid);
		
		assertTrue(p.getUid().equals(uid));
		assertTrue(p.getKeywords().contains("Algèbre linéaire"));
		assertTrue(p.getGreen().contains("rpn-id-1"));
	}
}




