package org.orioai.profile;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.orioai.profile.FormationService;
import org.orioai.profile.entity.Formation;
import org.orioai.profile.entity.MockData;

public class FormationServiceTest {
	
	static FormationService tempS;
	
	@BeforeClass
	public static void init(){
		Logger.getLogger("org.orioai").setLevel(Level.OFF);
		tempS = FormationService.getInstance();	
		tempS.setFormations(MockData.getFormations());
	}

	
	@Test
	public void testGetFormations(){	

		List<Formation> temps = tempS.getFormations();
	
		assertTrue(temps.size()==2);
		temps.get(0).getId().equals("IDFORM1");
		temps.get(1).getId().equals("IDFORM2");
		
		
	}
	
	@Test
	public void testGetFormationsWithId(){	
		List<String> idFormations = new ArrayList<String>();
		
		idFormations.add("IDFORM2");
		List<Formation> temps = tempS.getFormations(idFormations);
		assertTrue(temps.size()==1);
		assertTrue(idFormations.contains(temps.get(0).getId()));
		
		idFormations.add("IDFORM1");
		temps = tempS.getFormations(idFormations);
		assertTrue(temps.size()==2);
		assertTrue(idFormations.contains(temps.get(0).getId()));
		assertTrue(idFormations.contains(temps.get(1).getId()));
		
	}
	
	@Test
	public void testGetFormation(){	
		Formation temp = tempS.getFormation("IDFORM2");
		assertTrue(temp.getId().equals("IDFORM2"));
		
		temp = tempS.getFormation("toto");
		assertTrue(temp==null);
		
		temp = tempS.getFormation(null);
		assertTrue(temp==null);
	}
	
	
	
	
	
	
}
