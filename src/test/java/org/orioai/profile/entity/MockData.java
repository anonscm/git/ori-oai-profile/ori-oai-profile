package org.orioai.profile.entity;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.IOUtils;
import org.orioai.profile.ProfileServiceTest;
import org.orioai.profile.entity.Formation;
import org.orioai.profile.entity.RPN;
import org.orioai.profile.indexing.IndexingServiceSettings;
import org.orioai.profile.ldap.LDAPConfiguration;
import org.orioai.ws.indexing.IndexRecord;
import org.orioai.ws.indexing.IndexRecordFormat;
import org.orioai.ws.indexing.SearchResults;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class MockData {
	public static String getGoodID(){
		return "univ-ori-1";
	}
	
	public static String getGoodID2(){
		return "univ-ori-2";
	}
	
	
	public static String getGoodTitle(){
		return "Monosaccharides - Test A";
	}
	
	
	/**
	 * Recupere une fausse liste de RPN
	 * @param start index de la premiere RPN de la liste
	 * @param end index de la dernière RPN de la liste
	 * @return les RPN
	 */
	public IndexingResponse getMockRPNs(int start, int end){ 
		end=end+1;//on s'arrete a end+1

		List<RPN> rpns = new ArrayList<RPN>();
		for(int i=1;i<7;i++){
			RPN rpn = new RPN(i+"");
			rpn.authors.add("auteur du doc "+i);
			rpn.date="201"+i;
			rpn.description="description du doc "+i;
			rpn.title="titre du doc "+i;
			rpn.url="url du doc "+i;
			rpns.add(rpn);
		}

		if(end>rpns.size())
			end=rpns.size();

		List<RPN> rpnsReturn = new ArrayList<RPN>();
		if(start<rpns.size()){
			for(int i=start;i<end;i++){
				rpns.add(rpns.get(i));
			}
		}
			
		return new IndexingResponse(rpns.size(), rpnsReturn);
	}

	public static List<Formation> getFormations(){
		Formation temp = new Formation("IDFORM1","formation1",
				Arrays.asList("501.1","111"),
				Arrays.asList("Algèbre linéaire",MockData.getGoodTitle()));
		temp.setGreen(Arrays.asList("rpn-id-1"));
		Formation temp2 = new Formation("IDFORM2","formation2",
				Arrays.asList("502 5"),Arrays.asList("économie"));
		return Arrays.asList(temp,temp2);
	}
	
	
	public static LDAPConfiguration getLDAPConfig(){
		return new LDAPConfiguration(getLDAPProperties());
	}
	
	public static String getGoodUID(){
		return "euclid";
	}
	
	public static Properties getLDAPProperties(){
		Properties props= new Properties();
		props.setProperty("LDAP_PROTOCOLE", "ldap");
		props.setProperty("LDAP_SERVER", "ldap.forumsys.com:389");
		props.setProperty("LDAP_BASE", "dc=example,dc=com");
		props.setProperty("LDAP_PEOPLE", "");
		props.setProperty("LDAP_UID", "uid");
		props.setProperty("LDAP_LOGIN", "cn=read-only-admin,dc=example,dc=com");
		props.setProperty("LDAP_PASS", "password");
		
		return props;
	}
	
	public static Attributes getLDAPAttributes(){
		Attributes attrs = new BasicAttributes();	
		BasicAttribute ba =new BasicAttribute(getLDAPFormationAttr(), 
				new String("{1:}IDFORM1-00"));
		attrs.put(ba);
		BasicAttribute ba2 =new BasicAttribute(getLDAPDisplayNameAttr(), 
				new String("Toto Titi"));
		attrs.put(ba2);
		return attrs;
	}
	
	public static String getLDAPFormationAttr(){
		return "idformation";
	}
	public static String getLDAPDisplayNameAttr(){
		return "displayname";
	}
	
	public static String getIndexingResponseSXD(){
		return getFileContent("/indexingSXD.xml");
	}
	
	public static String getIndexingResponseSFA(){

		return getFileContent("/indexingSFA.xml");
	}
	
	public static String getLOMExample(){
		return getFileContent("/lom-example.xml");
		 	
	}
	
	public static String getTypeLomExemple(){
		return getFileContent("/types.xml");
	}
	
	public static String getFileContent(String path){
		InputStream input = ProfileServiceTest.class.getResourceAsStream(path);
		String result=null;
		try {
			result = IOUtils.toString(input, StandardCharsets.UTF_8.name());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	

	
	public static Document getDocument(String xml) {
		Document doc=null;
		try {
			doc = DocumentBuilderFactory.newInstance()
			        .newDocumentBuilder()
			        .parse(new InputSource(new StringReader(xml)));
		} catch (SAXException | IOException | ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return doc;
	}
	
	public static Document getDocument() throws ParserConfigurationException{
		DocumentBuilderFactory fabrique = DocumentBuilderFactory.newInstance(); 
		DocumentBuilder constructeur=null;
		Document document=null;
	
		constructeur = fabrique.newDocumentBuilder();
		document = constructeur.newDocument(); 
		return document;
	}
	
	public static IndexRecord getRecord1(){
		
		IndexRecordFormat irf = new IndexRecordFormat(IndexingServiceSettings.NAMESPACE_LOM);
		irf.setXmlContent(getIndexingResponseSFA());
		IndexRecord ir = new IndexRecord(MockData.getGoodID(), irf, "public");
				
		return ir;
	}
	
	public static IndexRecord getRecord2(){
		
		IndexRecordFormat irf = new IndexRecordFormat(IndexingServiceSettings.NAMESPACE_LOM);
		irf.setXmlContent(getIndexingResponseSXD());
		IndexRecord ir = new IndexRecord(MockData.getGoodID2(), irf, "public");
		
		return ir;
	}
	
	
	public static SearchResults getSearchResults(){
		SearchResults sr =new SearchResults();
		sr.setNumberOfResults(2);
		sr.setResults(Arrays.asList(getRecord1(),getRecord2()));
		return sr;
	}

	
	

}
