package org.orioai.ws.rest;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import javax.ws.rs.core.Response;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.orioai.profile.FormationService;
import org.orioai.profile.entity.MockData;
import org.orioai.profile.indexing.IndexingClient;
import org.orioai.profile.indexing.IndexingService;
import org.orioai.profile.indexing.MockIndexingInterface;
import org.orioai.ws.indexing.OriOaiIndexingServiceInterface;
import org.orioai.ws.rest.RPNWebInterface;


public class RPNWebInterfaceTest {

	static RPNWebInterface rwi;
	
	
	
	@BeforeClass
	public static void init(){
		Logger.getLogger("org.orioai").setLevel(Level.OFF);
		OriOaiIndexingServiceInterface inter= new MockIndexingInterface();
		IndexingClient ic = new IndexingClient(inter);
		IndexingService  is= new IndexingService(ic);
		
		FormationService ts = FormationService.getInstance();
		rwi = new RPNWebInterface();
		ts.setFormations(MockData.getFormations());
		RPNWebInterface.setIndexingService(is);
		RPNWebInterface.setFormationService(ts);
		
	}
	
	@Test 
	public void testGetRPN(){

		Response res = rwi.getRPN(MockData.getGoodID());
		assertTrue(res.getStatus()==200);
		res = rwi.getRPN("fail");
		assertTrue(res.getStatus()==404);
		res = rwi.getRPN(null);
		assertTrue(res.getStatus()==404);
	}
	
	
	@Test
	public void testSearchRPN(){
		Response res = rwi.searchRPN(Arrays.asList("Monosaccharides"),1,1);
		assertTrue(res.getStatus()==200);
		res = rwi.searchRPN(Arrays.asList("fail"),1,1);
		assertTrue(res.getStatus()==404);
		res = rwi.searchRPN(null,1,1);
		assertTrue(res.getStatus()==404);
	
	}
	
	@Test
	public void testGetRPNForFormation(){
		Response resp = rwi.getRPNForFormation("IDFORM1", 0, 0);
		assertTrue(resp.getStatus()==200);
		resp = rwi.getRPNForFormation("IDFORM1", 0, 19);
		assertTrue(resp.getStatus()==200);
		resp = rwi.getRPNForFormation("IDFORM1", 1, 0);
		assertTrue(resp.getStatus()==400);
		resp = rwi.getRPNForFormation("IDFORM1", 5, 2);
		assertTrue(resp.getStatus()==400);
		resp = rwi.getRPNForFormation("toto", 1, 1);
		assertTrue(resp.getStatus()==404);
		
	}
	

}
