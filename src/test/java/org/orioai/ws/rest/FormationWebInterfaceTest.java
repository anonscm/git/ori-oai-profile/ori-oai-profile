package org.orioai.ws.rest;

import static org.junit.Assert.assertTrue;

import javax.ws.rs.core.Response;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.orioai.profile.FormationService;
import org.orioai.profile.entity.MockData;
import org.orioai.ws.rest.FormationWebInterface;

public class FormationWebInterfaceTest {
	static FormationWebInterface fwi;

	
	@BeforeClass
	public static void init(){
		Logger.getLogger("org.orioai").setLevel(Level.OFF);
		FormationService fs = FormationService.getInstance();
		fs.setFormations(MockData.getFormations());
		fwi =  new FormationWebInterface();
		fwi.setFormationService(fs);
	}
	
	
	@Test
	public void testGetAllFormations(){
		
		Response res = fwi.getAllFormations();
		assertTrue(res.getStatus()==200);
	}
	
	
	@Test
	public void testGetFormation(){
		Response res = fwi.getFormation("IDFORM1");
		assertTrue(res.getStatus()==200);
		res = fwi.getFormation("toto");
		assertTrue(res.getStatus()==404);
		
	}
	

}
