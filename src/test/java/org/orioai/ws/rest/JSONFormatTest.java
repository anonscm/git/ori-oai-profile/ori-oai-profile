package org.orioai.ws.rest;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.orioai.profile.entity.Formation;
import org.orioai.profile.entity.RPN;
import org.orioai.ws.rest.JSONFormat;

public class JSONFormatTest {
	
	@Test
	public void testGetFormation(){
		Formation temp = new Formation("idform","master toto",
				Arrays.asList("501.3","601 41"),
				Arrays.asList("gestion de projet","économie"));
		
		JSONObject json = JSONFormat.getFormation(temp);
		
		assertTrue(json.get("id").equals("idform"));
		assertTrue(json.get("name").equals("master toto"));
		assertTrue(json.getJSONArray("deweys").length()==2);
		assertTrue(json.getJSONArray("keywords").length()==2);
		
	}
	@Test
	public void testGetRPN(){
		RPN rpn = new RPN("1");
		rpn.authors.add("auteur");
		rpn.types.add("exercise");
		rpn.title="toto";
		JSONObject json = JSONFormat.getRPN(rpn);
		assertTrue(json.get("id").equals("1"));		
		assertTrue(json.get("title").equals("toto"));
	}
	
	@Test
	public void testGetFormations(){
		List<Formation> temps = Arrays.asList(
				new Formation("id1","form1"), 
				new Formation("id2","form2")
				);
		JSONArray json  = JSONFormat.getFormations(temps);
		assertTrue(json.length()==2);
	}
	
	@Test
	public void testGetRPNFormation(){
		
		JSONObject json = JSONFormat.getRPNFormation("idform1",5, new ArrayList<RPN>());
		assertTrue(json.length()==3);
		
	}

}
